# Smart arm
Code used to implement the genetic algorithm Map-Elite to a robotic arm. This
work was done at ISIR, Paris, under the direction of S. Doncieux and A. Cully,
and was an application of the paper from Nature 'Robots that can adapt like
animals'.  
The code has been done in 2015, it needs some cleansing, which is in progress.
Please come back later.

## Description
- map_elite: Contains the crustcrawler_arm code: generate a map with map-elite
thank's to a given simulation
- limbo: Contains the crustcrawler_arm code for limbo : use an archive given by
Map-Elite to reach a target
- visualization: Few scripts to visualize archives


## Notes to myself
Sferes, fork 273f1f0 (06.20.14), https://github.com/sferes2/nn2


## Visualization
![Example of visualization interface](interface.png)  
[![Modelisation](https://img.youtube.com/vi/FSn_YeAiHAs/0.jpg)](https://youtu.be/FSn_YeAiHAs)  
![Photo Robotic Arm](arm_photo.jpg)
