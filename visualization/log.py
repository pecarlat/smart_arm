#################################################################################################################
# log.py																										#
# Shows few information about the map																			#
# 																												#
# python log.py archive_10000.dat																				#
#################################################################################################################

import matplotlib
matplotlib.use('TkAgg')
import numpy as np
import matplotlib.pyplot as plt
import Tix

from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import matplotlib.gridspec as gridspec

from mpl_toolkits.mplot3d import Axes3D
from itertools import product, combinations

import sys
if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk


if(len(sys.argv) != 2):
    print 'Wrong argument number, please respect the following format :'
    print 'python log.py archive_10000.dat -> for having a look of stats relative to generation 10 000'
    sys.exit(0)





""""""""""""""""""""""""""""""""""""""""""""""""""
""" GLOBAL VARIABLES                           """
""""""""""""""""""""""""""""""""""""""""""""""""""
execfile("parameters.py")



""""""""""""""""""""""""""""""""""""""""""""""""""
""" CLASSES                                    """
""""""""""""""""""""""""""""""""""""""""""""""""""
class Stat(object):
    def __init__(self, whole_map_nb_samples, whole_map_nb_positions, whole_map_nb_samples_on_table, whole_map_nb_positions_on_table, \
                       found_nb_samples, found_nb_positions, found_nb_samples_on_table, found_nb_positions_on_table, \
                       found_waypoint_repartition):
        self.whole_map_nb_samples = whole_map_nb_samples                        # Samples in the whole map
        self.whole_map_nb_positions = whole_map_nb_positions                    # Positions in the whole map
        self.whole_map_nb_samples_on_table = whole_map_nb_samples_on_table      # Samples existing on table
        self.whole_map_nb_positions_on_table = whole_map_nb_positions_on_table  # Positions existing on table
        self.found_nb_samples = found_nb_samples                                # Samples found by Map-Elite
        self.found_nb_positions = found_nb_positions                            # Positions found by Map-Elite
        self.found_nb_samples_on_table = found_nb_samples_on_table              # Samples on table found by Map-Elite
        self.found_nb_positions_on_table = found_nb_positions_on_table          # Positions on table found by Map-Elite
        self.found_waypoint_repartition = found_waypoint_repartition            # Number of waypoints in the controller of each sample


class Data:
    global archive_path          # the path where to find the archive
    global nb_archive            # the number of the archive
    
    global archive               # [type : tab<string>] the text of the archive
    global data_archive          # [type : tab< tab<float> >] all the keys of the archive
    global performance_archive   # [type : tab<float>] the performances of the archive
    
    global pos_archive           # [type : tab< tab<float> >] all the keys of the archive
    global pos_perf_archive      # [type : tab<float>] the average of performance for each position
    global pos_perf_mc_archive   # [type : tab<float>] the number of samples for this position
    
    global stats                 # [type : Stat] main stats of the archive
    
    global discretization        # [type : float] size (in meters of a slice = sizes_z/dim_z)
    global actual_slice          # [type : int] the number of the slice currently printed
    global data_sliced           # [type : tab< tab< tab<float> > >] all the keys of all the slices of the archive
    global pos_sliced            # [type : tab< tab< tab<float> > >] all the keys of all positions in the archive's slices
    
    global actual_color     # The wished color
    
    
    # Initialization of Data
    def __init__(self):
        self.archive_path = ''
        self.nb_archive = 0
        
        self.archive = []
        self.data_archive = []
        for i in range(0, map_dim):
            self.data_archive.append([])
        self.performance_archive = []
        
        self.pos_archive = []
        for i in range(0, map_dim):
            self.pos_archive.append([])
        self.pos_perf_archive = []
        self.pos_perf_mc_archive = []
        
        self.stats = Stat(1, 1, 1, 1, \
                          0, 0, 0, 0, \
                          [])
        
        self.discretization = sizes[2]/float(dims[2])
        self.actual_slice = 0
        self.data_sliced = []
        self.pos_sliced = []
        for i in range(0, dims[2]):
            tmp = []
            for j in range(0, map_dim + 2):
                tmp.append([])
            self.data_sliced.append(tmp)
            tmp.append([])
            self.pos_sliced.append(tmp)
        
        self.actual_color = 0
    
    
    # Recover data from simple archive
    def recover_archive(self, archiveName):
        self.archive_path = archiveName[:archiveName.find('archive_')]
        s = archiveName[:-(len(archiveName)-archiveName.find('.dat'))]
        s = s[s.find('archive_')+8:]
        self.nb_archive = int(s)
        text = open(archiveName, 'r')
        for line in text:
            self.archive.append(line)
        
        flag = False
        for i in range(0, len(self.archive)):
            tmp = self.archive[i].split()
            for j in range(0, map_dim):
                if(j < 3):
                    self.data_archive[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
                else:
                    self.data_archive[j].append(float(tmp[j]))
            self.performance_archive.append(float(tmp[map_dim]))
            
            # If it is a new position (and not only a new sample)
            if(flag == False or self.pos_archive[0][len(self.pos_archive[0])-1] != self.data_archive[0][i] \
                             or self.pos_archive[1][len(self.pos_archive[1])-1] != self.data_archive[1][i] \
                             or self.pos_archive[2][len(self.pos_archive[2])-1] != self.data_archive[2][i]):
                flag = True
                for j in range(0, map_dim):
                    if(j < 3):
                        self.pos_archive[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
                    else:
                        self.pos_archive[j].append(float(tmp[j]))
                self.pos_perf_archive.append(float(tmp[map_dim]))
                self.pos_perf_mc_archive.append(1)
            else:
                self.pos_perf_archive[len(self.pos_perf_mc_archive)-1] += float(tmp[map_dim])
                self.pos_perf_mc_archive[len(self.pos_perf_mc_archive)-1] += 1
        
        # Mean of all samples' performances of each position
        for i in range(0, len(self.pos_perf_archive)):
            self.pos_perf_archive[i] /= float(self.pos_perf_mc_archive[i])
    
    
    # Update the stats
    def update_stats(self):
        eps = sys.float_info.epsilon
        
        for i in range(0, map_dim):
            self.stats.whole_map_nb_samples *= dims[i]
            if(i < 3):
                self.stats.whole_map_nb_positions *= dims[i]
        
        alpha = []
        beta = []
        for i in range(0, 2): # positions in the grid but not in the table for x and y
            alpha.append(gaps[i] + (sizes[i]/2.0) - (table_size[i]/2.0))
            beta.append(- gaps[i] + (sizes[i]/2.0) - dist_arm_cube[i] + table_pos[i] - (table_size[i]/2.0))
            if(alpha[i] < 0):
                alpha[i] = 0
            else:
                alpha[i] = round((alpha[i]*dims[i])/sizes[i])
            if(beta[i] < 0):
                beta[i] = 0
            else:
                beta[i] = round((beta[i]*dims[i])/sizes[i])
        
        self.stats.whole_map_nb_positions_on_table = (dims[0]-alpha[0]-beta[0])*(dims[1]-alpha[1]-beta[1])*2
        self.stats.whole_map_nb_samples_on_table = self.stats.whole_map_nb_positions_on_table
        for i in range(3, map_dim):
            self.stats.whole_map_nb_samples_on_table *= dims[i]
        
        self.stats.found_nb_samples = len(self.data_archive[0])
        self.stats.found_nb_positions = len(self.pos_archive[0])
        
        for i in range(0, 2):
            alpha[i] = (alpha[i]*sizes[i])/float(dims[i])
            beta[i] = (beta[i]*sizes[i])/float(dims[i])
        
        for i in range(0, self.stats.found_nb_samples):
            tmp = []
            tmp.append((self.data_archive[0][i]-dist_arm_cube[0]-gaps[0]+(sizes[0]/2.0))/float(sizes[0]))
            tmp.append((self.data_archive[1][i]-dist_arm_cube[1]-gaps[1]+(sizes[1]/2.0))/float(sizes[1]))
            tmp.append((self.data_archive[2][i]-dist_arm_cube[2]-gaps[2]+(sizes[2]/2.0))/float(sizes[2]))
            if(tmp[0] >= beta[0] and tmp[0] <= 1 - alpha[0] \
                and tmp[1] >= beta[1] and tmp[1] <= 1 - alpha[1] \
                and tmp[2] >= (table_size[2]-eps) and tmp[2] <= (table_size[2]+(sizes[2]/float(dims[2]))+eps)):
                self.stats.found_nb_samples_on_table += 1
        
        for i in range(0, self.stats.found_nb_positions):
            tmp = []
            tmp.append((self.pos_archive[0][i]-dist_arm_cube[0]-gaps[0]+(sizes[0]/2.0))/float(sizes[0]))
            tmp.append((self.pos_archive[1][i]-dist_arm_cube[1]-gaps[1]+(sizes[1]/2.0))/float(sizes[1]))
            tmp.append((self.pos_archive[2][i]-dist_arm_cube[2]-gaps[2]+(sizes[2]/2.0))/float(sizes[2]))
            if(tmp[0] >= beta[0] and tmp[0] <= 1 - alpha[0] \
                and tmp[1] >= beta[1] and tmp[1] <= 1 - alpha[1] \
                and tmp[2] >= (table_size[2]-eps) and tmp[2] <= (table_size[2]+(sizes[2]/float(dims[2]))+eps)):
                self.stats.found_nb_positions_on_table += 1
        
        for i in range(0, self.stats.found_nb_samples):
            tmp = self.archive[i].split()
            self.stats.found_waypoint_repartition.append((len(tmp)-map_dim-1)/waypoint_size)
    
    
    # Returns the index of the slice where is zVal
    def isInSlice(self, zVal):
        tmp = round(zVal*dims[2])
        if(tmp == dims[2]):
            tmp -= 1
        return int(tmp)
    
    
    # Recover divided data
    def recover_slices(self):
        for i in range(0, len(self.data_archive[0])):
            for j in range(0, map_dim):
                self.data_sliced[self.isInSlice(self.data_archive[2][i])][j].append(self.data_archive[j][i])
            self.data_sliced[self.isInSlice(self.data_archive[2][i])][map_dim].append(self.performance_archive[i])
            self.data_sliced[self.isInSlice(self.data_archive[2][i])][map_dim+1].append(i)
        for i in range(0, len(self.pos_archive[0])):
            for j in range(0, map_dim):
                self.pos_sliced[self.isInSlice(self.pos_archive[2][i])][j].append(self.pos_archive[j][i])
            self.pos_sliced[self.isInSlice(self.pos_archive[2][i])][map_dim].append(self.pos_perf_archive[i])
            self.pos_sliced[self.isInSlice(self.pos_archive[2][i])][map_dim+1].append(self.pos_perf_mc_archive[i])



# Class App
class App:
    global data              # Data of the archive
    
    global figVisu           # Figure for visu
    global axVisu            # Axes for visu figure
    global canvasVisu        # Canvas for visu figure
    global figCutVisu        # Figure for cutVisu
    global axCutVisu         # Axes for cutVisu figure
    global canvasCutVisu     # Canvas for cutVisu figure
    global figEvolution      # Figure for evolution
    global axEvolution       # Axes for evolution figure
    global canvasEvolution   # Canvas for evolution figure
    
    
    def __init__(self, data, master):
        
        self.data = data
        
        
        # Create a container
        frame = Tk.Frame(master)
        
        
        # TOP FRAME = Figure Visu + Performance buttons / Evolution / Stats
        # Located in top
        topFrame = Tk.Frame(frame)
        
        visuFrame = Tk.Frame(topFrame)
        self.figVisu = Figure(figsize = (4, 4), dpi = 100)
        self.canvasVisu = FigureCanvasTkAgg(self.figVisu, master = visuFrame)
        self.canvasVisu.show()
        self.canvasVisu.get_tk_widget().pack(side = Tk.TOP, fill = Tk.NONE, expand = 0)
        self.axVisu = self.figVisu.add_subplot(111, projection = '3d')
        self.routineVisu(self.data.actual_color)
        
        selectPerformanceFrame = Tk.Frame(visuFrame)
        self.button_none = Tk.Button(selectPerformanceFrame, text = "None", command = self.performanceNone)
        self.button_none.pack(side = Tk.LEFT)
        self.button_real = Tk.Button(selectPerformanceFrame, text = "Real performance", command = self.performanceReal)
        self.button_real.pack(side = Tk.LEFT)
        self.button_macrocells = Tk.Button(selectPerformanceFrame, text = "Number of macrocells filled", command = self.performanceMacrocells)
        self.button_macrocells.pack(side = Tk.LEFT)
        selectPerformanceFrame.pack(side = Tk.BOTTOM)
        
        visuFrame.pack(side = Tk.LEFT)
        
        
        self.figEvolution = Figure(figsize = (8, 4), dpi = 100)
        self.canvasEvolution = FigureCanvasTkAgg(self.figEvolution, master = topFrame)
        self.canvasEvolution.show()
        self.canvasEvolution.get_tk_widget().pack(side = Tk.LEFT, fill = Tk.NONE, expand = 0)
        self.axEvolution = []
        gs1 = gridspec.GridSpec(3, 1)
        for i in range(0, 3):
            self.axEvolution.append(self.figEvolution.add_subplot(gs1[i]))
        self.routineEvolution(self.data.nb_archive)
        
        
        self.stats = Tk.Label(topFrame, text = self.getStats(), anchor = Tk.W, justify = Tk.LEFT)
        self.stats.pack(side = Tk.LEFT)
        
        topFrame.pack(side = Tk.TOP)
        
        
        
        # BOTTOM FRAME = Figure cutVisu + Slices buttons / Controllers screen
        # Located in top
        bottomFrame = Tk.Frame(frame)
        
        cutVisuFrame = Tk.Frame(bottomFrame)
        
        self.figCutVisu = Figure(figsize = (5.6, 4), dpi = 100)
        self.canvasCutVisu = FigureCanvasTkAgg(self.figCutVisu, master = cutVisuFrame)
        self.canvasCutVisu.show()
        self.canvasCutVisu.get_tk_widget().pack(side = Tk.TOP, fill = Tk.NONE, expand = 0)
        posx = dist_arm_cube[0] + gaps[0]
        posy = dist_arm_cube[1] + gaps[1]
        diff = (max(sizes[0], sizes[1]) - sizes[0]) / 2.0
        self.axCutVisu = self.figCutVisu.add_subplot(111, xlim = (posx - (sizes[0]/2.0) - 0.2 - diff, posx + (sizes[0]/2.0) + 0.2 + diff), \
                                                          ylim = (posy - (sizes[1]/2.0) - 0.2 - diff,  posy + (sizes[1]/2.0) + 0.2 + diff))
        self.figCutVisu.canvas.mpl_connect('button_release_event', self.getCoords)
        self.routineCutVisu(self.data.actual_color)
        
        selectSliceFrame = Tk.Frame(cutVisuFrame)
        self.button_quit = Tk.Button(selectSliceFrame, text = "-", command = self.pressLeft)
        self.button_quit.pack(side = Tk.LEFT)
        self.current = Tk.Label(selectSliceFrame, text = 'slice ' + str(self.data.actual_slice+1) + '/' + str(dims[2]), relief = Tk.RAISED)
        self.current.pack(side = Tk.LEFT)
        self.button_quit = Tk.Button(selectSliceFrame, text = "+", command = self.pressRight)
        self.button_quit.pack(side = Tk.LEFT)
        selectSliceFrame.pack(side = Tk.BOTTOM)
        
        cutVisuFrame.pack(side = Tk.LEFT)
        
        textFrame = Tk.Frame(bottomFrame)
        scrollbar = Tk.Scrollbar(textFrame)
        scrollbar.pack(side = Tk.RIGHT, fill = Tk.Y)
        self.pointsToShow = Tk.Text(textFrame, height = 27, width = 160, yscrollcommand = scrollbar.set)
        self.pointsToShow.insert(Tk.END, '')
        self.pointsToShow.pack(side = Tk.TOP)
        textFrame.pack(side = Tk.LEFT)
        scrollbar.config(command = self.pointsToShow.yview)
        
        bottomFrame.pack()
        
        
        frame.pack()
    
    
    # Get stats
    def getStats(self):
        stats = 'Global informations about the whole map' + '\n' + \
                'It contains ' + str(data.stats.whole_map_nb_samples) + ' different cells (all of them can\'t be filled)' + '\n' + \
                'It contains ' + str(data.stats.whole_map_nb_positions) + ' different positions (all of them can\'t be filled)' + '\n' + \
                str(int(data.stats.whole_map_nb_samples_on_table)) + ' samples are \'on the table\'\n' + \
                str(int(data.stats.whole_map_nb_positions_on_table)) + ' positions are \'on the table\'\n' + \
                '\n' + \
                'The archive contains ' + str(data.stats.found_nb_samples) + ' samples, that means ' + str(data.stats.found_nb_samples/float(data.stats.whole_map_nb_samples)*100) + '% of the global map' + '\n' + \
                'The archive contains ' + str(data.stats.found_nb_positions) + ' positions, that means ' + str(data.stats.found_nb_positions/float(data.stats.whole_map_nb_positions)*100) + '% of the global map' + '\n' + \
                str(data.stats.found_nb_samples_on_table) + ' of these samples are on the table, that means ' + str(data.stats.found_nb_samples_on_table/float(data.stats.whole_map_nb_samples_on_table)*100) + '% of the global map' + '\n' + \
                str(data.stats.found_nb_positions_on_table) + ' of these positions are on the table, that means ' + str(data.stats.found_nb_positions_on_table/float(data.stats.whole_map_nb_positions_on_table)*100) + '% of the global map' + '\n' + \
                '\n' + \
                'The \'longest\' controller is composed of ' + str(max(data.stats.found_waypoint_repartition)) + ' waypoints' + '\n' + \
                'The average of controllers\' size is (in number of waypoints) ' + str(sum(data.stats.found_waypoint_repartition)/float(len(data.stats.found_waypoint_repartition))) + ' waypoints' + '\n'
        return stats
    
    # Draw cube
    def drawCube(self, ax, r1, r2, r3):
        for s, e in combinations(np.array(list(product(r1,r2,r3))), 2):
            if np.sum(np.abs(s-e)) == r1[1]-r1[0] or np.sum(np.abs(s-e)) == r2[1]-r2[0] or np.sum(np.abs(s-e)) == r3[1]-r3[0]:
                ax.plot3D(*zip(s,e), color="b")
    
    
    # Routine for visu
    def routineVisu(self, p):
        posx = dist_arm_cube[0]+gaps[0]
        posy = dist_arm_cube[1]+gaps[1]
        diff = (max(sizes[0], sizes[1])-sizes[0])/2.0
        
        self.axVisu.zaxis.set_major_locator(LinearLocator(10))
        self.axVisu.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        self.axVisu.set_xlabel('x')
        self.axVisu.set_ylabel('y')
        self.axVisu.set_zlabel('z')
        self.axVisu.grid(False)
        self.axVisu.set_xlim(posx - (sizes[0]/2.0) - 0.2 - diff, posx + (sizes[0]/2.0) + 0.2 + diff)
        self.axVisu.set_ylim(posy - (sizes[1]/2.0) - 0.2 - diff,  posy + (sizes[1]/2.0) + 0.2 + diff)
        self.drawCube(self.axVisu, [table_pos[0]-(table_size[0]/2.0), table_pos[0]+(table_size[0]/2.0)], \
                                   [-(table_size[1]/2.0), (table_size[1]/2.0)], \
                                   [0, table_size[2]/sizes[2]])
        self.drawCube(self.axVisu, [dist_arm_cube[0]-(cube_size/2.0), dist_arm_cube[0]+(cube_size/2.0)], \
                                   [dist_arm_cube[1]-(cube_size/2.0), dist_arm_cube[1]+(cube_size/2.0)], \
                                   [table_size[2]/sizes[2], table_size[2]/sizes[2]+cube_size/sizes[2]])
        self.drawCube(self.axVisu, [-0.12, 0.12], \
                                   [-0.12, 0.12], \
                                   [table_size[2]/sizes[2], table_size[2]/sizes[2]+0.1])
        
        if(p == 0):
            self.axVisu.scatter(self.data.pos_archive[0], self.data.pos_archive[1], self.data.pos_archive[2], marker = 'o')
        elif(p == 1):
            self.axVisu.scatter(self.data.pos_archive[0], self.data.pos_archive[1], self.data.pos_archive[2], c = self.data.pos_perf_archive, marker = 'o')
        elif(p == 2):
            self.axVisu.scatter(self.data.pos_archive[0], self.data.pos_archive[1], self.data.pos_archive[2], c = self.data.pos_perf_mc_archive, marker = 'o')
        
        self.axVisu.plot_surface(self.data.data_archive[0], self.data.data_archive[1], self.data.data_archive[2], rstride = 1, cstride = 1, cmap = cm.RdBu, linewidth = 0, antialiased = False)
    
    
    # Routine for cutVisu
    def routineCutVisu(self, p):
        eps = sys.float_info.epsilon
        if(((self.data.actual_slice+1)*self.data.discretization)-eps < table_size[2]/sizes[2]):
            polygon = plt.Polygon([[table_pos[0]-(table_size[0]/2.0), +(table_size[1]/2.0)], \
                                   [table_pos[0]-(table_size[0]/2.0), -(table_size[1]/2.0)], \
                                   [table_pos[0]+(table_size[0]/2.0), -(table_size[1]/2.0)], \
                                   [table_pos[0]+(table_size[0]/2.0), +(table_size[1]/2.0)]], hatch='\\', fill=False)
            self.axCutVisu.add_patch(polygon)
        if(((self.data.actual_slice+1)*self.data.discretization)+eps >= table_size[2]/sizes[2] and ((self.data.actual_slice+1)*self.data.discretization)-eps < table_size[2]/sizes[2]+cube_size/sizes[2]):
            polygon = plt.Polygon([[dist_arm_cube[0]-(cube_size/2.0), -(cube_size/2.0)], \
                                   [dist_arm_cube[0]-(cube_size/2.0), +(cube_size/2.0)], \
                                   [dist_arm_cube[0]+(cube_size/2.0), +(cube_size/2.0)], \
                                   [dist_arm_cube[0]+(cube_size/2.0), -(cube_size/2.0)]], hatch='\\', fill=False)
            self.axCutVisu.add_patch(polygon)
        if(((self.data.actual_slice+1)*self.data.discretization)+eps >= table_size[2]/sizes[2] and ((self.data.actual_slice+1)*self.data.discretization)-eps < table_size[2]/sizes[2]+0.1):
            polygon = plt.Polygon([[-0.12, 0.12], [-0.12, -0.12], [0.12, -0.12], [0.12, 0.12]], hatch='\\', fill=False)
            self.axCutVisu.add_patch(polygon)
        self.axCutVisu.set_autoscale_on(False)
        
        if self.data.data_sliced[self.data.actual_slice]:
            if(p == 0):
                self.axCutVisu.scatter(self.data.pos_sliced[self.data.actual_slice][0], self.data.pos_sliced[self.data.actual_slice][1], alpha=0.5)
            elif(p == 1):
                self.axCutVisu.scatter(self.data.pos_sliced[self.data.actual_slice][0], self.data.pos_sliced[self.data.actual_slice][1], c = self.data.pos_sliced[self.data.actual_slice][map_dim], vmin = min(self.data.pos_perf_archive), vmax = max(self.data.pos_perf_archive))
            elif(p == 2):
                sc = self.axCutVisu.scatter(self.data.pos_sliced[self.data.actual_slice][0], self.data.pos_sliced[self.data.actual_slice][1], c = self.data.pos_sliced[self.data.actual_slice][map_dim+1], vmin = min(self.data.pos_perf_mc_archive), vmax = max(self.data.pos_perf_mc_archive))
    
    
    # Routine for evolution
    def routineEvolution(self, nbArchive):
        valx = []
        valy = []
        
        for i in range(0, nbArchive+1, dump_period):
            f = open(self.data.archive_path+'archive_'+str(i)+'.dat', 'r')
            array = []
            for line in f:
                array.append(line)
            
            performance = []
            nb_wp = []
            for j in range(0, len(array)):
                tmp = array[j].split()
                performance.append(float(tmp[map_dim])-0.5)
                nb_wp.append((len(tmp)-map_dim-1)/waypoint_size)
            
            valx.append(i)
            for k in range(0, 3):
                valy.append([])
            nb_of_cells = 1
            
            for k in range(0, map_dim):
                nb_of_cells *= dims[k]
            valy[0].append(len(performance)*100/float(nb_of_cells))
            valy[1].append(sum(performance)/float(len(performance)))
            valy[2].append(sum(nb_wp)/float(len(nb_wp)))
        
        self.axEvolution[0].plot(valx, valy[0])
        self.axEvolution[0].get_xaxis().set_visible(False)
        self.axEvolution[0].set_title('Evolution of map filling')
        self.axEvolution[1].plot(valx, valy[1])
        self.axEvolution[1].get_xaxis().set_visible(False)
        self.axEvolution[1].set_title('Evolution of performance')
        self.axEvolution[2].plot(valx, valy[2])
        self.axEvolution[2].set_title('Evolution of number of waypoints')
    
    
    # Returns a list of all plots closer to the mouse position
    def getCloser(self, _x, _y):
        eps = sys.float_info.epsilon
        l = []
        precision_x = 1/float(dims[0]) # Underlying fluctuation range step
        precision_y = 1/float(dims[1])
        
        for i in range(0, len(self.data.data_sliced[self.data.actual_slice][0])):
            if(abs(self.data.data_sliced[self.data.actual_slice][0][i]-_x) < precision_x-eps and abs(self.data.data_sliced[self.data.actual_slice][1][i]-_y) < precision_y-eps):
                l.append(self.data.data_sliced[self.data.actual_slice][map_dim+1][i])
        return l
    
    
    # Prints the coordinates of closest plots when click
    def getCoords(self, e):
        _x = e.xdata
        _y = e.ydata
        
        if(_x < dist_arm_cube[0]+gaps[0]-(sizes[0]/2.0) or _x > dist_arm_cube[0]+gaps[0]+(sizes[0]/2.0) \
            or _y < dist_arm_cube[1]+gaps[1]-(sizes[1]/2.0) or _y > dist_arm_cube[1]+gaps[1]+(sizes[1]/2.0)):
            return
        
        liste = self.getCloser(_x, _y)
        
        t = ''
        for i in range(0, len(liste)):
            t += self.data.archive[liste[i]]
        self.pointsToShow.delete('0.0', Tk.END)
        self.pointsToShow.insert(Tk.END, t)
        self.pointsToShow.pack()
    
    
    # Move to next slice with keyboard press (left and right)
    def pressLeft(self):
        if(self.data.actual_slice != 0):
            self.axCutVisu.clear()
            self.data.actual_slice -= 1
            self.routineCutVisu(self.data.actual_color)
            self.canvasCutVisu.draw()
            self.current.config(text = 'slice ' + str(self.data.actual_slice+1) + '/' + str(dims[2]))
    
    def pressRight(self):
        if(self.data.actual_slice != dims[2]-1):
            self.axCutVisu.clear()
            self.data.actual_slice += 1
            self.routineCutVisu(self.data.actual_color)
            self.canvasCutVisu.draw()
            self.current.config(text = 'slice ' + str(self.data.actual_slice+1) + '/' + str(dims[2]))
    
    
    # Switch the visu figure according to the whished performance
    def performanceNone(self):
        self.axVisu.clear()
        self.axCutVisu.clear()
        self.data.actual_color = 0
        self.routineVisu(self.data.actual_color)
        self.routineCutVisu(self.data.actual_color)
        self.canvasVisu.draw()
        self.canvasCutVisu.draw()
    
    def performanceReal(self):
        self.axVisu.clear()
        self.axCutVisu.clear()
        self.data.actual_color = 1
        self.routineVisu(self.data.actual_color)
        self.routineCutVisu(self.data.actual_color)
        self.canvasVisu.draw()
        self.canvasCutVisu.draw()
    
    def performanceMacrocells(self):
        self.axVisu.clear()
        self.axCutVisu.clear()
        self.data.actual_color = 2
        self.routineVisu(self.data.actual_color)
        self.routineCutVisu(self.data.actual_color)
        self.canvasVisu.draw()
        self.canvasCutVisu.draw()



""""""""""""""""""""""""""""""""""""""""""""""""""
""" MAIN CODE                                  """
""""""""""""""""""""""""""""""""""""""""""""""""""

window = Tk.Tk()
window.wm_title("Visualization")

# Class that contains all important informations
data = Data()
# Recover the contain of the given archive and store it in Data
data.recover_archive(sys.argv[1])
# Split the map in slices and store them in Data
data.recover_slices()
# Get the main stats related to the archive
data.update_stats()


app = App(data, window)

window.mainloop()



