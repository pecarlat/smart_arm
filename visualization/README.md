# parameters.py
It is the code with global parameters, it is not launchable but is called by
all the others programs. The main parameter is concerning the size of the map:
map_dim. When you change it, don't forget to change the size of dims[] below.

# log.py
It is the code that provides the most exhaustive view of the archive, it shows
a 3d-view of the map, a interactive 2d view of each slices, the main stats of
the archive and the evolution observed from generation 0 to current generation.
It doesn't work if the folder where is archive_xxx.dat doesn't contain the
previous generations.  
You can:
- have an interactive 3d view of the map in three different colors : (1) None,
(2) Real performance (= the performance given by Map-Elite or the average of
performances given by Map-Elite for this position if map_dim > 3), (3)
Macrocells performance (= the number of cells filled in this position (usefull
only when map_dim > 3))
- have a 2d view of all slices of the map in the three different colors seen
previously + possibility of switch the slice with the two buttons + view of
controllers corresponding to a point when clicking on it
- have a view of the evolution of generations (map filling / performance /
average of number of waypoints by controller)
- have a view of main stats concerning the archive

# visu.py
It is the code that only calls the visu figure with a colorbar and with the
possibility of resize it / save it as a image.

# evolution.py
Permits to generate graphs independantly, to zoom on them, and to save them as
images.

# compareVersion.py
It is the code used to compare an archive after few tests done with map_elite /
modif_archive code. It permits, for example, to compare the map obtained with
the cube's mass = 1 and a map obtained with the cube's mass = 3

# viewDifference.py
Permits to see the difference between the normal simulation and the modified
one (colors represent the between the expected position (normal simu) and the
real position (modified simu) for this controller. So, if	I see a point, it
means that it exists a controller that reach that point in the modified
simulation, and more blue it is, more it corresponds to the expected
controller.