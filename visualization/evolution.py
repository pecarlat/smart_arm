#################################################################################################################
# evolution.py																									#
# Shows the evolution of the map across generations																#
# Options :																										#
# python evolution.py --option archive_10000.dat (shows the wanted evolution from gen 0 to gen 10000) 			#
# option --map_filling (also -mf) : shows the filling of the map												#
# option --performance (also -p) : shows the evolution of the performance										#
# option --nb_of_waypoints (also -nw) : shows the evolution of the number of waypoints							#
# option --all (also -a) : shows all the graphs																	#
# option --help (also -h) : shows all these options																#
#################################################################################################################

import matplotlib
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from itertools import product, combinations
from matplotlib.ticker import FuncFormatter


""""""""""""""""""""""""""""""""""""""""""""""""""
""" GLOBAL VARIABLES                           """
""""""""""""""""""""""""""""""""""""""""""""""""""
execfile("parameters.py")

if(len(sys.argv) == 2 and ('--help' in sys.argv[1]) or ('-h' in sys.argv[1])):
    printHelp()
    sys.exit(0)
elif(len(sys.argv) != 3):
    print 'Please respect the following format'
    print 'python evolution.py --options gen_min gen_max'
    print '--help for more information'
    sys.exit(0)


""""""""""""""""""""""""""""""""""""""""""""""""""
""" OTHER FUNCTIONS                            """
""""""""""""""""""""""""""""""""""""""""""""""""""
def printHelp():
    print "evolution.py"
    print "Shows the evolution of the map across generations"
    print
    print "python evolution.py --option archive_10000.dat (shows the wanted evolution from generation 0 to generation 10000)"
    print "option --map_filling (also -mf) : shows the filling of the map"
    print "option --performance (also -p) : shows the evolution of the performance"
    print "option --nb_of_waypoints (also -nw) : shows the evolution of the number of waypoints"
    print "option --help (also -h) : shows all these options"

def to_percent(y, position):
    return str(y) + '%'



""""""""""""""""""""""""""""""""""""""""""""""""""
""" MAIN CODE                                  """
""""""""""""""""""""""""""""""""""""""""""""""""""
valx = []
valy = []

archive_path = sys.argv[2][:sys.argv[2].find('archive_')]
s = sys.argv[2][:-(len(sys.argv[2])-sys.argv[2].find('.dat'))]
s = s[s.find('archive_')+8:]
nb_archive = int(s)

for i in range(0, nb_archive+1, dump_period):
    f = open(archive_path+'archive_'+str(i)+'.dat', 'r')
    array = []
    for line in f:
        array.append(line)
    
    performance = []
    nb_wp = []
    for j in range(0, len(array)):
        tmp = array[j].split()
        performance.append(float(tmp[map_dim])-0.5)
        nb_wp.append((len(tmp)-map_dim-1)/waypoint_size)
    
    valx.append(i)
    if ('--map_filling' in sys.argv[1]) or ('-mf' in sys.argv[1]):
        nb_of_cells = 1
        for k in range(0, map_dim):
            nb_of_cells *= dims[k]
        valy.append(len(performance)*100/float(nb_of_cells))
        
    elif ('--performance' in sys.argv[1]) or ('-p' in sys.argv[1]):
        valy.append(sum(performance)/float(len(performance)))
        
    elif ('--nb_of_waypoints' in sys.argv[1]) or ('-nw' in sys.argv[1]):
        valy.append(sum(nb_wp)/float(len(nb_wp)))
        
    elif ('--all' in sys.argv[1]) or ('-a' in sys.argv[1]):
        for k in range(0, 3):
            valy.append([])
        nb_of_cells = 1
        for k in range(0, map_dim):
            nb_of_cells *= dims[k]
        valy[0].append(len(performance)*100/float(nb_of_cells))
        valy[1].append(sum(performance)/float(len(performance)))
        valy[2].append(sum(nb_wp)/float(len(nb_wp)))
        
    elif ('--help' in sys.argv[1]) or ('-h' in sys.argv[1]):
        printHelp()
        sys.exit(-1);
        
    else:
        print 'Wrong option, -h for help'
        sys.exit(-1)


if ('--all' in sys.argv[1]) or ('-a' in sys.argv[1]):
    f, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
    ax1.plot(valx, valy[0])
    ax1.set_title('Evolution of map filling')
    ax2.plot(valx, valy[1])
    ax2.set_title('Evolution of performance')
    ax3.plot(valx, valy[2])
    ax3.set_title('Evolution of number of waypoints')
else:
    fig = plt.figure()
    ax = fig.add_subplot(111, xlim=(0, nb_archive), ylim=(min(valy), max(valy)))
    plt.plot(valx, valy)

plt.xlabel('number of generation')
if ('--map_filling' in sys.argv[1]) or ('-mf' in sys.argv[1]):
    formatter = FuncFormatter(to_percent)
    plt.gca().yaxis.set_major_formatter(formatter)
    plt.ylabel('percentage of map filled')
    plt.title('Evolution of map filling')
elif ('--performance' in sys.argv[1]) or ('-p' in sys.argv[1]):
    plt.ylabel('performance observed')
    plt.title('Evolution of performance')
elif ('--nb_of_waypoints' in sys.argv[1]) or ('-nw' in sys.argv[1]):
    plt.ylabel('average of waypoints observed')
    plt.title('Evolution of waypoints\' number')

plt.show()


