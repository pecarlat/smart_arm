# Values relatives to the grid in map-elite
map_dim = 3                    # dimension of the map [RQ : len(dims) = map_dim]
dims = [24, 16, 20]            # dimensions of the physical grid [RQ : len(dims) = map_dim]
# dims = [24, 16, 20, 10, 8]
# dims = [24, 16, 20, 6, 4, 5]

waypoint_size = 7              # size of a waypoint
sizes = [1.2, 0.8, 1]          # sizes of the physical grid (in meters)
gaps = [0.3, 0, 0.5]           # position of the center of the grid to the cube
dump_period = 50               # number of generation needed before being writed in archives

# Values relatives to the simulation
dist_arm_cube = [0.45, 0, 0]   # distance between the arm and the cube
table_pos = [0.45, 0, 0]       # distance between the arm and the center of the table
table_size = [1.6, 0.8, 0.2]   # sizes of the table
cube_size = 0.052              # size of the cube

# Values relatives to the performance
variance = True                # the performance is based on the variance (False = torque)
mean = True                    # the performance is the mean of variance/torque (False = sum)
pain = False                   # the arm can receive punition (False = cannot)


