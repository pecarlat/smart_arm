#################################################################################################################
# viewDifference.py																								#
# Permits to see the difference between the normal simulation and the modified one (colors represent the 		#
# between the expected position (normal simu) and the real position (modified simu) for this controller. So, if	#
# I see a point, it means that it exists a controller that reach that point in the modified simulation, and 	#
# more blue it is, more it corresponds to the expected controller.												#
# Options :																										#
# python viewDifference.py --option archive_10000.dat coeff1 													#
# option --mass (-m) : the difference depends on the mass														#
# option --rota (-r) : the difference depends on the rotation													#
#################################################################################################################

import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from itertools import product, combinations


if(len(sys.argv) != 4):
    print 'Wrong argument number, please respect the following format :'
    print 'python viewDifference.py --option archive_10000.dat x -> for having a look of the map in generation 10 000 with coeff x'
    print 'Options :'
    print '\toption --mass (-m) : coeff about mass'
    print '\toption --rota (-r) : coeff about rotation'
    sys.exit(0)


""""""""""""""""""""""""""""""""""""""""""""""""""
""" GLOBAL VARIABLES                           """
""""""""""""""""""""""""""""""""""""""""""""""""""
execfile("parameters.py")

data = []
data_diff = []
pos = []
pos_diff = []
for i in range(0, map_dim):
    data.append([])
    data_diff.append([])
    pos.append([])
    pos_diff.append([])
performance = []
pos_performance = []
pos_performance_mc = []

distances = []
pos_distances = []


""""""""""""""""""""""""""""""""""""""""""""""""""
""" FUNCTIONS                                  """
""""""""""""""""""""""""""""""""""""""""""""""""""
# Draw cube
def drawCube(ax, r1, r2, r3):
    for s, e in combinations(np.array(list(product(r1,r2,r3))), 2):
        if np.sum(np.abs(s-e)) == r1[1]-r1[0] or np.sum(np.abs(s-e)) == r2[1]-r2[0] or np.sum(np.abs(s-e)) == r3[1]-r3[0]:
            ax.plot3D(*zip(s,e), color="b")

def recoverArchive(archiveName):
    f = open(archiveName, 'r')
    array = []
    for line in f:
        array.append(line)
    
    flag = False
    for i in range(0, len(array)):
        tmp = array[i].split()
        for j in range(0, map_dim):
            if(j < 3):
                data[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
            else:
                data[j].append(float(tmp[j]))
        performance.append(float(tmp[map_dim]))
        
        # If it is a new position (and not only a new sample)
        if(flag == False or pos[0][len(pos[0])-1] != data[0][i] \
                         or pos[1][len(pos[1])-1] != data[1][i] \
                         or pos[2][len(pos[2])-1] != data[2][i]):
            flag = True
            for j in range(0, map_dim):
                if(j < 3):
                    pos[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
                else:
                    pos[j].append(float(tmp[j]))
            pos_performance.append(float(tmp[map_dim]))
            pos_performance_mc.append(1)
        else:
            pos_performance[len(pos_performance_mc)-1] += float(tmp[map_dim])
            pos_performance_mc[len(pos_performance_mc)-1] += 1
    
def distance(a, b):
    x = a[0]-b[0]
    y = a[1]-b[1]
    z = a[2]-b[2]
    return math.sqrt(x*x + y*y + z*z)

def recoverArchiveDiff(archiveName):
    f = open(archiveName, 'r')
    array = []
    for line in f:
        array.append(line)
    
    flag = False
    cpt = 0
    for i in range(0, len(array)):
        tmp = array[i].split()
        for j in range(0, map_dim):
            if(j < 3):
                data_diff[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
            else:
                data_diff[j].append(float(tmp[j]))
        
        # If it is a new position (and not only a new sample)
        if(flag == False or pos_diff[0][len(pos_diff[0])-1] != data_diff[0][i] \
                         or pos_diff[1][len(pos_diff[1])-1] != data_diff[1][i] \
                         or pos_diff[2][len(pos_diff[2])-1] != data_diff[2][i]):
            flag = True
            for j in range(0, map_dim):
                if(j < 3):
                    pos_diff[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
                else:
                    pos_diff[j].append(float(tmp[j]))
            
            tmpA = []
            tmpB = []
            for j in range(0, 3):
                tmpA.append(pos[j][cpt])
                tmpB.append(pos_diff[j][cpt])
            cpt += 1
            distances.append(distance(tmpA, tmpB))


""""""""""""""""""""""""""""""""""""""""""""""""""
""" MAIN CODE                                  """
""""""""""""""""""""""""""""""""""""""""""""""""""
plt.ion()

posx = dist_arm_cube[0]+gaps[0]
posy = dist_arm_cube[1]+gaps[1]
diff = (max(sizes[0], sizes[1])-sizes[0])/2.0

plt.ion()
fig = plt.figure()

ax = fig.add_subplot(111, projection = '3d')
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
#ax.grid(False)
ax.set_xlim(posx - (sizes[0]/2.0) - 0.2 - diff, posx + (sizes[0]/2.0) + 0.2 + diff)
ax.set_ylim(posy - (sizes[1]/2.0) - 0.2 - diff,  posy + (sizes[1]/2.0) + 0.2 + diff)
drawCube(ax, [table_pos[0]-(table_size[0]/2.0), table_pos[0]+(table_size[0]/2.0)], \
                 [-(table_size[1]/2.0), (table_size[1]/2.0)], \
                 [0, table_size[2]/sizes[2]])
drawCube(ax, [dist_arm_cube[0]-(cube_size/2.0), dist_arm_cube[0]+(cube_size/2.0)], \
                 [dist_arm_cube[1]-(cube_size/2.0), dist_arm_cube[1]+(cube_size/2.0)], \
                 [table_size[2]/sizes[2], table_size[2]/sizes[2]+cube_size/sizes[2]])
drawCube(ax, [-0.12, 0.12], \
                 [-0.12, 0.12], \
                 [table_size[2]/sizes[2], table_size[2]/sizes[2]+0.1])


rota = True
if('--mass' in sys.argv[1]) or ('-m' in sys.argv[1]):
    rota = False
elif('--rota' in sys.argv[1]) or ('-r' in sys.argv[1]):
    rota = True
else:
    print 'Unknown option'
    sys.exit(-1)
if(rota == False):
    s = sys.argv[2][:-(len(sys.argv[2])-sys.argv[2].find('.dat'))] + '_mass_x' + str(sys.argv[3]) + '.dat'
else:
    s = sys.argv[2][:-(len(sys.argv[2])-sys.argv[2].find('.dat'))] + '_rota_x' + str(sys.argv[3]) + '.dat'
recoverArchive(sys.argv[2])
recoverArchiveDiff(s)

p = ax.scatter(pos_diff[0], pos_diff[1], pos_diff[2], c = distances, marker = 'o')
surf = ax.plot_surface(pos_diff[0], pos_diff[1], pos_diff[2], rstride = 1, cstride = 1, cmap = cm.RdBu, linewidth = 0, antialiased = False)

fig.colorbar(p, shrink=0.5, aspect=5, ax=ax)

plt.ioff()

plt.show()




