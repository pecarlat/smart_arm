#################################################################################################################
# visu.py																										#
# Shows the map in its globality																				#
# 																												#
# python visu.py archive_10000.dat x (shows the map at the generation with the color x)							#
#################################################################################################################

import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from itertools import product, combinations


if(len(sys.argv) != 3):
    print 'Wrong argument number, please respect the following format :'
    print 'python visu.py archive_10000.dat x -> for having a look of the map in generation 10 000 with color x'
    print '\tx = 0 : no color'
    print '\tx = 1 : real color'
    print '\tx = 2 : macrocells color'
    sys.exit(0)


""""""""""""""""""""""""""""""""""""""""""""""""""
""" GLOBAL VARIABLES                           """
""""""""""""""""""""""""""""""""""""""""""""""""""
execfile("parameters.py")

data = []
pos = []
for i in range(0, map_dim):
    data.append([])
    pos.append([])
performance = []
pos_performance = []
pos_performance_mc = []


""""""""""""""""""""""""""""""""""""""""""""""""""
""" FUNCTIONS                                  """
""""""""""""""""""""""""""""""""""""""""""""""""""
# Draw cube
def drawCube(ax, r1, r2, r3):
    for s, e in combinations(np.array(list(product(r1,r2,r3))), 2):
        if np.sum(np.abs(s-e)) == r1[1]-r1[0] or np.sum(np.abs(s-e)) == r2[1]-r2[0] or np.sum(np.abs(s-e)) == r3[1]-r3[0]:
            ax.plot3D(*zip(s,e), color="b")

def recoverArchive(archiveName):
    f = open(archiveName, 'r')
    array = []
    for line in f:
        array.append(line)
    
    flag = False
    for i in range(0, len(array)):
        tmp = array[i].split()
        for j in range(0, map_dim):
            if(j < 3):
                data[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
            else:
                data[j].append(float(tmp[j]))
        performance.append(float(tmp[map_dim]))
        
        # If it is a new position (and not only a new sample)
        if(flag == False or pos[0][len(pos[0])-1] != data[0][i] \
                         or pos[1][len(pos[1])-1] != data[1][i] \
                         or pos[2][len(pos[2])-1] != data[2][i]):
            flag = True
            for j in range(0, map_dim):
                if(j < 3):
                    pos[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
                else:
                    pos[j].append(float(tmp[j]))
            pos_performance.append(float(tmp[map_dim]))
            pos_performance_mc.append(1)
        else:
            pos_performance[len(pos_performance_mc)-1] += float(tmp[map_dim])
            pos_performance_mc[len(pos_performance_mc)-1] += 1


""""""""""""""""""""""""""""""""""""""""""""""""""
""" MAIN CODE                                  """
""""""""""""""""""""""""""""""""""""""""""""""""""
plt.ion()

posx = dist_arm_cube[0]+gaps[0]
posy = dist_arm_cube[1]+gaps[1]
diff = (max(sizes[0], sizes[1])-sizes[0])/2.0

plt.ion()
fig = plt.figure()

ax = fig.add_subplot(111, projection = '3d')
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
#ax.grid(False)
ax.set_xlim(posx - (sizes[0]/2.0) - 0.2 - diff, posx + (sizes[0]/2.0) + 0.2 + diff)
ax.set_ylim(posy - (sizes[1]/2.0) - 0.2 - diff,  posy + (sizes[1]/2.0) + 0.2 + diff)
drawCube(ax, [table_pos[0]-(table_size[0]/2.0), table_pos[0]+(table_size[0]/2.0)], \
                 [-(table_size[1]/2.0), (table_size[1]/2.0)], \
                 [0, table_size[2]/sizes[2]])
drawCube(ax, [dist_arm_cube[0]-(cube_size/2.0), dist_arm_cube[0]+(cube_size/2.0)], \
                 [dist_arm_cube[1]-(cube_size/2.0), dist_arm_cube[1]+(cube_size/2.0)], \
                 [table_size[2]/sizes[2], table_size[2]/sizes[2]+cube_size/sizes[2]])
drawCube(ax, [-0.12, 0.12], \
                 [-0.12, 0.12], \
                 [table_size[2]/sizes[2], table_size[2]/sizes[2]+0.1])


recoverArchive(sys.argv[1])

# Mean of all samples' performances of each position
for i in range(0, len(pos_performance)):
    pos_performance[i] /= float(pos_performance_mc[i])

col = True
if(int(sys.argv[2]) == 0):
    col = False
    p = ax.scatter(pos[0], pos[1], pos[2], marker = 'o')
elif(int(sys.argv[2]) == 1):
    p = ax.scatter(pos[0], pos[1], pos[2], c = pos_performance, marker = 'o')
elif(int(sys.argv[2]) == 2):
    p = ax.scatter(pos[0], pos[1], pos[2], c = pos_performance_mc, marker = 'o')

surf = ax.plot_surface(pos[0], pos[1], pos[2], rstride = 1, cstride = 1, cmap = cm.RdBu, linewidth = 0, antialiased = False)

if(col):
    fig.colorbar(p, shrink=0.5, aspect=5, ax=ax)

plt.ioff()

plt.show()




