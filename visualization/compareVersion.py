#################################################################################################################
# compareVersion.py																								#
# Compare different view of an archive (different cube mass or cube rotation)									#
# Options :																										#
# python compareVersion.py --option archive_10000.dat coeff1 coeff2 ... 										#
# option --mass (-m) : the difference depends on the mass														#
# option --rota (-r) : the difference depends on the rotation													#
#################################################################################################################

import matplotlib
matplotlib.use('TkAgg')
import numpy as np
import matplotlib.pyplot as plt
import Tix
import math

from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import matplotlib.gridspec as gridspec

from mpl_toolkits.mplot3d import Axes3D
from itertools import product, combinations

import sys
if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk


if(len(sys.argv) < 4):
    print 'Wrong argument number, please respect the following format :'
    print 'python compareVersion.py --option archive_10000.dat 1.5 2 ... -> for having a look of stats relative to generation 10 000'
    print 'Options :'
    print '\toption --mass (-m) : difference between maps focused on mass : archive_xxx_mass_xcoeff.dat'
    print '\toption --rota (-r) : difference between maps focused on rotation : archive_xxx_rota_xcoeff.dat'
    sys.exit(0)



""""""""""""""""""""""""""""""""""""""""""""""""""
""" GLOBAL VARIABLES                           """
""""""""""""""""""""""""""""""""""""""""""""""""""
execfile("parameters.py")



""""""""""""""""""""""""""""""""""""""""""""""""""
""" CLASSES                                    """
""""""""""""""""""""""""""""""""""""""""""""""""""
class Stat(object):
    def __init__(self, whole_map_nb_samples, whole_map_nb_positions, whole_map_nb_samples_on_table, whole_map_nb_positions_on_table, \
                       found_nb_samples, found_nb_positions, found_nb_samples_on_table, found_nb_positions_on_table, \
                       found_waypoint_repartition):
        self.whole_map_nb_samples = whole_map_nb_samples
        self.whole_map_nb_positions = whole_map_nb_positions
        self.whole_map_nb_samples_on_table = whole_map_nb_samples_on_table
        self.whole_map_nb_positions_on_table = whole_map_nb_positions_on_table
        self.found_nb_samples = found_nb_samples
        self.found_nb_positions = found_nb_positions
        self.found_nb_samples_on_table = found_nb_samples_on_table
        self.found_nb_positions_on_table = found_nb_positions_on_table
        self.found_waypoint_repartition = found_waypoint_repartition

class Data:
    global archive_path
    global nb_archive
    
    global archive
    global data_archive
    global data_archive_diff
    global performance_archive
    global performance_archive_diff
    
    global distances
    
    global stats
    
    global discretization
    global actual_slice
    global data_sliced
    
    
    def __init__(self):
        self.archive_path = ''
        self.nb_archive = 0
        
        self.archive = []
        self.archive_diff = []
        
        self.data_archive = []
        for i in range(0, map_dim):
            self.data_archive.append([])
        
        self.performance_archive = []
        
        self.max_nb_samples = 0
        self.max_nb_positions = 0
        self.nb_samples = 0
        self.nb_waypoints = []
        self.max_nb_samples_on_table = 0
        self.nb_samples_on_table = 0
        
        self.stats = Stat(1, 1, 1, 1, \
                          0, 0, 0, 0, \
                          [])
        
        self.discretization = 1/float(dims[2])
        self.actual_slice = 0
        self.data_sliced = []
        for i in range(0, dims[2]):
            tmp = []
            for j in range(0, map_dim + 2):
                tmp.append([])
            self.data_sliced.append(tmp)
        
        self.distances = []
    
    
    # Recover data from simple archive
    def recover_archive(self, archiveName):
        self.archive_path = archiveName[:archiveName.find('archive_')]
        s = archiveName[:-(len(archiveName)-archiveName.find('.dat'))]
        s = s[s.find('archive_')+8:]
        self.nb_archive = int(s)
        text = open(archiveName, 'r')
        for line in text:
            self.archive.append(line)
        
        for i in range(0, len(self.archive)):
            tmp = self.archive[i].split()
            for j in range(0, map_dim):
                if(j < 3):
                    self.data_archive[j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
                else:
                    self.data_archive[j].append(float(tmp[j]))
            self.performance_archive.append(float(tmp[map_dim]))
    
    # Recover data from archive diff
    def prepareArchiveDiff(self, nb):
        self.data_archive_diff = []
        self.performance_archive_diff = []
        for i in range(0, nb):
            tmp = []
            for j in range(0, map_dim):
                tmp.append([]) 
            self.data_archive_diff.append(tmp)
            self.performance_archive_diff.append([])
    
    def distance(self, a, b):
        x = a[0]-b[0]
        y = a[1]-b[1]
        z = a[2]-b[2]
        return math.sqrt(x*x + y*y + z*z)
    
    # Recover data from archive diff
    def recover_archive_diff(self, archiveName, coeff, nb, rotation):
        tmpDist = []
        if(rotation == False):
            s = archiveName[:-(len(archiveName)-archiveName.find('.dat'))] + '_mass_x' + str(coeff) + '.dat'
        else:
            s = archiveName[:-(len(archiveName)-archiveName.find('.dat'))] + '_rota_x' + str(coeff) + '.dat'
        text = open(s, 'r')
        archive_diff = []
        for line in text:
            archive_diff.append(line)
        
        for i in range(0, len(archive_diff)):
            tmp = archive_diff[i].split()
            for j in range(0, map_dim):
                if(j < 3):
                    self.data_archive_diff[nb][j].append(float(tmp[j])*sizes[j]+dist_arm_cube[j]+gaps[j]-sizes[j]/2.0)
                else:
                    self.data_archive_diff[nb][j].append(float(tmp[j]))
            self.performance_archive_diff[nb].append(float(tmp[map_dim]))
            
            tmpA = []
            tmpB = []
            for j in range(0, 3):
                tmpA.append(self.data_archive[j][i])
                tmpB.append(self.data_archive_diff[nb][j][i])
            tmpDist.append(self.distance(tmpA, tmpB))
        self.distances.append(tmpDist)
            

# Class App
class App:
    global data
    
    global figures
    global canvas
    global axs
    
    
    def __init__(self, data, master):
        posx = dist_arm_cube[0]+gaps[0]
        posy = dist_arm_cube[1]+gaps[1]
        diff = (max(sizes[0], sizes[1])-sizes[0])/2.0
        
        self.data = data
        
        # Create a container
        frame = Tk.Frame(master)
        
        self.figures = []
        self.canvas = []
        self.axs = []
        
        # FIGURE VISU
        self.figures.append(Figure(figsize=(4,4), dpi=100))
        self.canvas.append(FigureCanvasTkAgg(self.figures[0], master=frame))
        self.canvas[0].show()
        self.canvas[0].get_tk_widget().pack(side=Tk.LEFT, fill=Tk.NONE, expand=0)
        self.axs.append(self.figures[0].add_subplot(111, projection = '3d', \
                                               xlim=(posx - (sizes[0]/2.0) - 0.2 - diff, posx + (sizes[0]/2.0) + 0.2 + diff), \
                                               ylim=(posy - (sizes[1]/2.0) - 0.2 - diff,  posy + (sizes[1]/2.0) + 0.2 + diff)))
        self.routineVisu(self.data.data_archive, self.data.performance_archive, 0)
        self.axs[0].set_title('normale map')
        
        for i in range(0, len(self.data.data_archive_diff)):
            self.figures.append(Figure(figsize=(4,4), dpi=100))
            self.canvas.append(FigureCanvasTkAgg(self.figures[i+1], master=frame))
            self.canvas[i+1].show()
            self.canvas[i+1].get_tk_widget().pack(side=Tk.LEFT, fill=Tk.NONE, expand=0)
            self.axs.append(self.figures[i+1].add_subplot(111, projection = '3d', \
                                                   xlim=(posx - (sizes[0]/2.0) - 0.2 - diff, posx + (sizes[0]/2.0) + 0.2 + diff), \
                                                   ylim=(posy - (sizes[1]/2.0) - 0.2 - diff,  posy + (sizes[1]/2.0) + 0.2 + diff)))
            self.routineVisu(self.data.data_archive_diff[i], self.data.distances[i], i+1)
            self.axs[i+1].set_title('map with x' + str(sys.argv[3+i]))
        
        frame.pack()
    
    # Draw cube
    def drawCube(self, ax, r1, r2, r3):
        for s, e in combinations(np.array(list(product(r1,r2,r3))), 2):
            if np.sum(np.abs(s-e)) == r1[1]-r1[0] or np.sum(np.abs(s-e)) == r2[1]-r2[0] or np.sum(np.abs(s-e)) == r3[1]-r3[0]:
                ax.plot3D(*zip(s,e), color="b")
    
    # Routine for visu
    def routineVisu(self, data, perf, nb):
        self.axs[nb].zaxis.set_major_locator(LinearLocator(10))
        self.axs[nb].zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        self.axs[nb].set_xlabel('x')
        self.axs[nb].set_ylabel('y')
        self.axs[nb].set_zlabel('z')
        self.drawCube(self.axs[nb], [table_pos[0]-(table_size[0]/2.0), table_pos[0]+(table_size[0]/2.0)], \
                                   [-(table_size[1]/2.0), (table_size[1]/2.0)], \
                                   [0, table_size[2]/sizes[2]])
        self.drawCube(self.axs[nb], [dist_arm_cube[0]-(cube_size/2.0), dist_arm_cube[0]+(cube_size/2.0)], \
                                   [dist_arm_cube[1]-(cube_size/2.0), dist_arm_cube[1]+(cube_size/2.0)], \
                                   [table_size[2]/sizes[2], table_size[2]/sizes[2]+cube_size/sizes[2]])
        self.drawCube(self.axs[nb], [-0.12, 0.12], \
                                   [-0.12, 0.12], \
                                   [table_size[2]/sizes[2], table_size[2]/sizes[2]+0.1])
        self.axs[nb].scatter(data[0], data[1], data[2], c=perf, marker='o')
        self.axs[nb].plot_surface(data[0], data[1], data[2], rstride=1, cstride=1, cmap=cm.RdBu, linewidth=0, antialiased=False)



""""""""""""""""""""""""""""""""""""""""""""""""""
""" MAIN CODE                                  """
""""""""""""""""""""""""""""""""""""""""""""""""""

fenetre = Tk.Tk()
fenetre.wm_title("Visualization")

if('--mass' in sys.argv[1]) or ('-m' in sys.argv[1]):
    rota = False
elif('--rota' in sys.argv[1]) or ('-r' in sys.argv[1]):
    rota = True
else:
    print 'Unknown option'
    sys.exit(-1)

data = Data()
data.recover_archive(sys.argv[2])
data.prepareArchiveDiff(len(sys.argv)-3)
for i in range(3, len(sys.argv)):
    data.recover_archive_diff(sys.argv[2], sys.argv[i], i-3, rota) # 1.5 is the coeff, have to see how we want to give it


app = App(data, fenetre)

fenetre.mainloop()



