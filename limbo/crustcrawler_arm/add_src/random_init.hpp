#ifndef RANDOM_INIT_HPP_
#define RANDOM_INIT_HPP_

namespace limbo {
	namespace init_functions {
		template<typename Params>
		// Chose Params::init::nb_samples random samples and try them to initialize the estimation map
		struct RandomSamplingArchiveBL {
			template<typename F, typename Opt>
			void operator()(const F& feval, Opt& opt) const {
				srand(time(NULL));
				
				std::cout << "Init phase" << std::endl;
				
				for (int i(0) ; i < Params::init::nb_samples() ; i++) {
					auto item = Params::archiveparams::archive.begin();
					std::advance(item, rand()%Params::archiveparams::archive.size());
					
					Eigen::VectorXd new_sample(item->first.size());
					for(int j(0) ; j < item->first.size() ; j++)
						new_sample[j] = item->first[j];
					
					std::cout << "random sample : " << new_sample.transpose() << std::endl;
					try {
						opt.add_new_sample(new_sample, feval(new_sample));
					} catch(int e) {
						std::cout << "no data, black listed" << std::endl;
						opt.add_new_bl_sample(new_sample);
					}
				}
				std::cout << std::endl;
			}
		};
	}
}

#endif


