#ifndef STOPPING_TOLERANCE_HPP_
#define STOPPING_TOLERANCE_HPP_

#include <Eigen/Core>
#include <vector>
#include "../parameters_simulation.hpp"

//USING_PART_OF_NAMESPACE_EIGEN

namespace limbo	{
	namespace stopping_criterion {
	
		float vectorXdDistance(Eigen::VectorXd v, Eigen::VectorXd t) {
			t[0] *= Params_Simu::grid::size_x;
			v[0] *= Params_Simu::grid::size_x;
			t[1] *= Params_Simu::grid::size_y;
			v[1] *= Params_Simu::grid::size_y;
			t[2] *= Params_Simu::grid::size_z;
			v[2] *= Params_Simu::grid::size_z;
			return -((t-v).norm());
		}
		
		template<typename Params>
		bool compareVectorXdDistance(Eigen::VectorXd i, Eigen::VectorXd j) {
			return vectorXdDistance(i, Params::target::point) < vectorXdDistance(j, Params::target::point);
		}
		
		template<typename Params>
		struct MinTolerance {
			MinTolerance() {
				
			}
			
			template<typename BO>
			bool operator()(const BO& bo) {
				if(bo.observations().empty())
					return true;
				
				if(bo.observations()[0].size() > 1){
					return -((Params::target::point - *std::max_element(bo.observations().begin(), bo.observations().end(), compareVectorXdDistance<Params>)).norm()) <= Params::mintolerance::tolerance();
				} else {
					return -(bo.best_observation().norm()) <= Params::mintolerance::tolerance();
				}
			}
			
		protected:
			
		};
		
		template<typename Params>
		struct DataConfluence {
			DataConfluence() {
				
			}
			
			template<typename BO>
			bool operator()(const BO& bo) {
				if(bo.previous().sample == bo.current().sample && bo.previous().acquisition == bo.current().acquisition) {
					return false;
				}
				
				return true;
			}
			
		protected:
			
		};
	}
}

#endif
