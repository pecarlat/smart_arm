#ifndef GP_AUTO_MEAN_BL_HPP_
#define GP_AUTO_MEAN_BL_HPP_

#include <limbo/gp_auto.hpp>
#include <limbo/gp_auto_mean.hpp>

namespace limbo {
	namespace model {
		template<typename Params, typename KernelFunction, typename MeanFunction, typename ObsType=Eigen::VectorXd>
		class GPAutoMeanBlackList : public GPAutoMean<Params, KernelFunction, MeanFunction> {
			
		 public:
			
			// TODO : init KernelFunction with dim in GP
			GPAutoMeanBlackList() : GPAutoMean<Params, KernelFunction, MeanFunction>() {
				
			}

			GPAutoMeanBlackList(int dim_in, int dim_out) : GPAutoMean<Params, KernelFunction, MeanFunction>(dim_in, dim_out) {
				
			}
			
			void compute(const std::vector<Eigen::VectorXd>& samples, const std::vector<ObsType>& observations, double noise, const std::vector<Eigen::VectorXd>& bl_samples, bool flag = true) {
			
				GP<Params, KernelFunction, MeanFunction>::compute(samples, observations, noise);
				this->_optimize_likelihood();
				_bl_samples = bl_samples;
				
				this->_compute_obs_mean(); // ORDER MATTERS
				this->_compute_kernel();
				
				//std::cout<<"max coef obs: "<<this->_max_coef_obs.transpose()<<std::endl;
				//std::cout<<"max coef obs mean: "<<this->_max_coef_obs_mean.transpose()<<std::endl;
			}
			
			int nb_samples() const {
				return this->_samples.size();
			}
			
			int nb_bl_samples() const {
				return this->_bl_samples.size();
			}
			
			double sigma(const Eigen::VectorXd& v) const {
				if (this->_samples.size() == 0 && _bl_samples.size() == 0)
					return sqrt(this->_kernel_function(v, v));
				return _sigma(v, _compute_k_bl(v));
			}
			
			
		 protected:
			
			std::vector<Eigen::VectorXd> _bl_samples; // black listed samples
			Eigen::MatrixXd _inv_bl_kernel;

			void _compute_kernel() {
				// O(n^2) [should be negligible]
				this->_kernel.resize(this->_samples.size(), this->_samples.size());
				for (size_t i(0) ; i < this->_samples.size() ; i++)
					for (size_t j(0) ; j < this->_samples.size(); ++j)
						this->_kernel(i, j) = this->_kernel_function(this->_samples[i], this->_samples[j]) + ((i==j)?this->_noise:0);
				
				// O(n^3)
				// _inverted_kernel = _kernel.inverse();
				this->_llt = Eigen::LLT<Eigen::MatrixXd>(this->_kernel);

				// alpha = K^{-1} * this->_obs_mean;
				this->_alpha = this->_llt.matrixL().solve(this->_obs_mean);
				this->_llt.matrixL().adjoint().solveInPlace(this->_alpha);
				
				
	//------ ADD BLOCKWISE INVERSION FOR THE BLACKLIST -----//
				// K^{-1} using Cholesky decomposition
				Eigen::MatrixXd A1 = Eigen::MatrixXd::Identity(this->_samples.size(), this->_samples.size());
				this->_llt.matrixL().solveInPlace(A1);
				this->_llt.matrixL().transpose().solveInPlace(A1);
				
				_inv_bl_kernel.resize(this->_samples.size()+_bl_samples.size(), this->_samples.size()+_bl_samples.size());	
				if(_bl_samples.size()==0) {
					_inv_bl_kernel = A1;
					return;
				}
				
				Eigen::MatrixXd B(this->_samples.size(), _bl_samples.size());
				for(size_t i(0) ; i < this->_samples.size() ; i++)
					for(size_t j(0) ; j < _bl_samples.size() ; ++j)
						B(i,j) = this->_kernel_function(this->_samples[i], _bl_samples[j]);
				
				
				Eigen::MatrixXd D(_bl_samples.size(), _bl_samples.size());
				for(size_t i(0) ; i < _bl_samples.size() ; i++)
					for(size_t j(0) ; j < _bl_samples.size() ; ++j)
						D(i,j) = this->_kernel_function(_bl_samples[i], _bl_samples[j])+ ((i==j)?this->_noise:0);
				
				
				Eigen::MatrixXd comA = (D-B.transpose()*A1*B);
				Eigen::LLT<Eigen::MatrixXd> llt_bl(comA);
				Eigen::MatrixXd comA1 = Eigen::MatrixXd::Identity(_bl_samples.size(), _bl_samples.size());
				llt_bl.matrixL().solveInPlace(comA1);
				llt_bl.matrixL().transpose().solveInPlace(comA1);
				
				
				//fill the matrix block wise
				_inv_bl_kernel.block(0, 0, this->_samples.size(), this->_samples.size())=A1 + A1*B*comA1*B.transpose()*A1;
				_inv_bl_kernel.block(0, this->_samples.size(), this->_samples.size(), _bl_samples.size())=-A1*B*comA1;
				_inv_bl_kernel.block(this->_samples.size(), 0, _bl_samples.size(), this->_samples.size())=_inv_bl_kernel.block(0,this->_samples.size(), this->_samples.size(),_bl_samples.size()).transpose();
				_inv_bl_kernel.block(this->_samples.size(), this->_samples.size(), _bl_samples.size(), _bl_samples.size())=comA1;
			}
			
			double _sigma(const Eigen::VectorXd& v, const Eigen::VectorXd& k) const {
				double res = this->_kernel_function(v, v) - k.transpose()*_inv_bl_kernel*k;
				return (res<=std::numeric_limits<double>::epsilon())?0:res;
				//return _kernel_function(v, v) - (k.transpose() * _inverted_kernel * k)[0];
			}
			
			Eigen::VectorXd _compute_k_bl(const Eigen::VectorXd& v) const {
				Eigen::VectorXd k = this->_compute_k(v);
				if(_bl_samples.size() == 0)
					return k;
				
				Eigen::VectorXd k_bl(this->_samples.size()+_bl_samples.size());
				// k_bl.block(0, 0, this->_samples.size(), 1) = k;
				k_bl.head(this->_samples.size()) = k;
				for(size_t i(0) ; i < _bl_samples.size() ; i++)
					k_bl[i+this->_samples.size()] = this->_kernel_function(_bl_samples[i], v);
				return k_bl;
			}
		};
	}
}
#endif
