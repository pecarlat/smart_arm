#ifndef UCB_MULTI_HPP_
#define UCB_MULTI_HPP_
namespace limbo
{
	namespace acquisition_functions
	{

		template<typename Params, typename Model>
		class UCB_explore {
		public:
			UCB_explore(const Model& model, int iteration = 0) : _model(model) {
			}
			size_t dim_in() const {
	return _model.dim_in();
			}
			size_t dim_out() const {
	return _model.dim_out();
			}
			double operator()(const Eigen::VectorXd& v) const {
	return	sqrt(_model.sigma(v));
			}
		protected:
			const Model& _model;
		};



		template<typename Params, typename Model>
		class UCB_multi {
		
		public:
			UCB_multi(const Model& model, int iteration = 0) : _model(model) {
				
			}
			
			size_t dim_in() const {
				return _model.dim_in();
			}
			
			size_t dim_out() const {
				return _model.dim_out();
			}
			
			double operator()(const Eigen::VectorXd& v) const {
				double res = -(Params::target::point - _model.mu(v)).norm()			// -dist(target - mu)
								+ Params::ucb::alpha() * sqrt(_model.sigma(v));		// + alpha * sigma_noise
				return res;
			}
			
		protected:
			const Model& _model;
		};
		
		template<typename Params, typename Model>
		class UCB_multi_polaire {
		public:
			UCB_multi_polaire(const Model& model, int iteration = 0) : _model(model) {
			}
			size_t dim() const {
	return _model.dim();
			}
			double operator()(const Eigen::VectorXd& v) const {
	
	Eigen::VectorXd p=_model.mu(v);
	double res= -sqrt(Params::target::point(0)*Params::target::point(0)+ p(0)*p(0) - 2*Params::target::point(0)*p(0)*cos(p(1)-Params::target::point(1)) ) + Params::ucb::alpha() * sqrt(_model.sigma(v));
	//	std::cout<<v.transpose()<< "	dist= "<<-(Params::target::point-_model.mu(v)).norm()<< " incert: "<<Params::ucb::alpha() * sqrt(_model.sigma(v))<<"	total: "<<res<<std::endl;
	return res;
			}
		protected:
			const Model& _model;
		};
		

	}
}

#endif


