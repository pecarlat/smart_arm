#ifndef MEAN_ARCHIVE_HPP_
#define MEAN_ARCHIVE_HPP_


namespace limbo {
	namespace mean_functions {
		template<typename Params>
		struct MeanArchive_Map {
			MeanArchive_Map(size_t dim_out = 1) {
				
			}
			
			template<typename GP>
			Eigen::VectorXd operator()(const Eigen::VectorXd& v, const GP&) const {
				std::vector<float> key(v.size(), 0);
				for(int i(0) ; i < v.size() ; i++)
					key[i] = v[i];
				
				return Eigen::Vector3d(Params::archiveparams::archive.at(key).object_state[0], 
										Params::archiveparams::archive.at(key).object_state[1], 
										Params::archiveparams::archive.at(key).object_state[2]);
			}
		};
	}
}

#endif
