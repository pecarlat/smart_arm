#ifndef BOPTIMIZER_BL_HPP_
#define BOPTIMIZER_BL_HPP_

#include <type_traits>
#include <limbo/bo_base.hpp>
#include "parameters_simulation.hpp"


namespace limbo {
	
	// The format for a result obtained in an iteration of the optimization
	struct _local_result {
		Eigen::VectorXd sample;
		float acquisition;
	};
	typedef _local_result local_result;
	
	// Distance between vectors v and t
	float vectorXdDistance(Eigen::VectorXd v, Eigen::VectorXd t) {
		return -((t-v).norm());
	}
	
	// Comparaison of two vectors, return true if vector i is nearer to the target than vector j 
	template<typename Params>
	bool compareVectorXdDistance(Eigen::VectorXd i, Eigen::VectorXd j) {
		return vectorXdDistance(i, Params::target::point) < vectorXdDistance(j, Params::target::point);
	}
	
	template <
		class Params
		, class A1 = boost::parameter::void_
		, class A2 = boost::parameter::void_
		, class A3 = boost::parameter::void_
		, class A4 = boost::parameter::void_
		, class A5 = boost::parameter::void_
		, class A6 = boost::parameter::void_
		, class A7 = boost::parameter::void_
		>
	class BOptimizerBL : public BoBase<Params, A1, A2, A3, A4, A5, A6, A7> {
		
	public:
		
		typedef BoBase<Params, A1, A2, A3, A4, A5, A6, A7> base_t;
		typedef typename base_t::obs_t obs_t;
		typedef typename base_t::model_t model_t;
		typedef typename base_t::inner_optimization_t inner_optimization_t;
		typedef typename base_t::acquisition_function_t acquisition_function_t;

		template<typename EvalFunction>
		void optimize(const EvalFunction& feval, bool reset = true) {
			
			// Clear samples, bl_samples, observations and launch init function (NoInit or RandomBLInit for example)
			this->_init(feval, reset);
			
			// ???
			_model = model_t(EvalFunction::dim_in, EvalFunction::dim_objective);
			
			// Prepare process
			if(this->_observations.size())
				_model.compute(this->_samples, this->_observations, Params::boptimizer::noise(), _bl_samples);
			
			inner_optimization_t inner_optimization;
			
			// While stopping criterions are not respected (max iteration, tolerance, and no doubles)
			bool free_pass = true;
			while(free_pass) {
				int it(this->_iteration);
				
				// Acquisition function, here UCB multi, 
				acquisition_function_t acqui(_model, this->_iteration);
				
				// Get next sample (sample that maximize acquisition function)
				Eigen::VectorXd new_sample = inner_optimization(acqui, acqui.dim_in());
				switchLocalResults(new_sample);
				
				try {
					// Try to add this new sample : new_sample to samples and feval(new_sample) (position) to observation -> src/bo_base.hpp
					this->add_new_sample(new_sample, feval(new_sample));
					_current.acquisition = acqui(this->_samples[this->_samples.size() - 1]);
					
					std::cout << "[results "<<it<<"]\t" << "-> key : " << this->_samples[this->_samples.size() - 1].transpose() << std::endl;
					std::cout << "[results "<<it<<"]\t" << "-> observe : " << this->_observations[this->_observations.size() - 1].transpose() << std::endl;
					std::cout << "[results "<<it<<"]\t" << "-> acquisition value : " << acqui(this->_samples[this->_samples.size() - 1]) << std::endl;
					std::cout << "[results "<<it<<"]\t" << "-> distance found : " << vectorXdDistance(this->_observations[this->_observations.size() - 1], Params::target::point) << std::endl;
				}
				catch(...) {
					std::cout << "["<<it<<"]\t" << "-> key : " << new_sample.transpose() << std::endl;
					std::cout << "["<<it<<"]\t" << "-> NO DATA, point black-listed" << std::endl;
					
					// If observation is not good (NaN or Inf), blacklist -> only the sample, not the observation
					add_new_bl_sample(new_sample);
				}
				
				if(!this->_observations.empty()) {
					_model.compute(this->_samples, this->_observations, Params::boptimizer::noise(), _bl_samples);
					this->_update_stats(*this);
					
					std::cout << "[best]\t" << "-> key : " << best_sample().transpose() << std::endl;
					std::cout << "[best]\t" << "-> observation : " << best_observation().transpose() << std::endl;
					std::cout << "[best]\t" << "-> distance : " << vectorXdDistance(best_observation(), Params::target::point) << std::endl;
				} else {
					if(_previous.sample == _current.sample && _previous.acquisition == _current.acquisition)
						break;
				}
				
				std::cout << std::endl;
				this->_iteration++;
				free_pass = this->_pursue(*this);
			}
		}
		
		
		// Add the sample s to the black-list
		void add_new_bl_sample(const Eigen::VectorXd& s) {
			_bl_samples.push_back(s);
		}
		
		// Returns the best observation found
		const obs_t& best_observation() const {
			if(!this->_observations.empty()) {
				return *std::max_element(this->_observations.begin(), this->_observations.end(), compareVectorXdDistance<Params>);
			} else {
				throw 42; // No observations
			}
		}
		
		// Returns the best sample found
		const Eigen::VectorXd& best_sample() const {
			if(!this->_observations.empty()) {
				auto max_e = std::max_element(this->_observations.begin(), this->_observations.end(), compareVectorXdDistance<Params>);
				return this->_samples[std::distance(this->_observations.begin(), max_e)];
			} else {
				throw 42; // No observations
			}
		}
		
		const local_result previous() const { return this->_previous; }
		const local_result current() const { return this->_current; }

		const model_t& model() const { return _model; }
		const std::vector<Eigen::VectorXd>& bl_samples() const { return _bl_samples; }
		
		
	protected:
		
		model_t _model;
		std::vector<Eigen::VectorXd> _bl_samples;
		local_result _current;
		local_result _previous;
		
		template<typename F>
		void _init(const F& feval, bool reset = true) {
			this->_iteration = 0;
			if(reset) {
				this->_samples.clear();
				this->_bl_samples.clear();
				this->_observations.clear();
			}
			
			this->_current.sample = Eigen::VectorXd(Params::global::map_dim);
			this->_previous.sample = Eigen::VectorXd(Params::global::map_dim);
			for(int i(0) ; i < Params::global::map_dim ; i++) {
				this->_current.sample[i] = -1;
				this->_previous.sample[i] = -2;
			}
			
			this->_current.acquisition = -1;
			this->_previous.acquisition = -2;
			
			if(this->_samples.empty() && this->_bl_samples.empty())
				typename base_t::init_function_t()(feval, *this);
		}
		
		void switchLocalResults(Eigen::VectorXd newSample) {
			_previous.sample = _current.sample;
			_previous.acquisition = _current.acquisition;
			_current.sample = newSample;
			_current.acquisition = 0;
		}
	};
}

#endif
