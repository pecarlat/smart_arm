# Limbo
We want to implement an algorithm that admits that it exists few differences
between two simulation (or between the simulation and the reality) and that
tries to find efficiently a new way to reach its target. For example, if I have
two different simulations, one where the cube has a mass of 1 and the other
where the cube has a mass of 3, I want to find a controller of the first
simulation (found in a map provided by MAP-Elite) that can works with the
second simulation as fast as possible.  
This optimization is mainly an implementation of the Bayesian Optimization and
is mostly inspired of A. Cully's works.


# Installation
NOT TESTED (Probably many things I forget, have to install it again in a clean
PC)
```
cd dream
git clone -b gp_multi https:://github.com/jbmouret/limbo.git
cd limbo_gp_multi
./waf configure --boost-libs /home/$USER/dream/lib/ --boost-include /home/$USER/dream/include/ --eigen /home/$USER/dream/include --prefix /home/$USER/dream/
./waf build
```
Put limbo/crustcrawler_arm in limbo_gp_multi/exp/ folder


# Launch
In dream/limbo_gp_multi/ folder
```
./waf --exp crustcrawler_arm --target main_rotation_bl
```

Many possibilities then :
- build/default/exp/crustcrawler_arm/main_rotation_bl archive_100.dat ->
optimization is launched with random target
- build/default/exp/crustcrawler_arm/main_rotation_bl archive_100.dat t1 t2 t3
... -> optimization is launched with the given target t1 t2 t3 ... (for now,
target is a 3 dimensional vector, corresponding to the expected finale position
of the cube)
- build/default/exp/crustcrawler_arm/main_rotation_bl archive_100.dat -k k1 k2
k3 ... -> shows information about the key (if the key belongs to this archive,
and what is the associated controller) [RQ : the key is a x dimensional vector,
x corresponding to the size of the map (for now, 3, 5 or 6), its size can be
changed by changind Params::global::map_dim]
- build/default/exp/crustcrawler_arm/main_rotation_bl -s wp1 wp2 wp3 ... ->
launch the simulation of a controller (variable number of waypoints allowed)
[RQ : a waypoint expects Params::global::waypoint_size float values (for now,
waypoint_size = 7)]


# General
A big part of limbo used exactly the same files as MAP-Elite : differences are
located in the main.cpp, in boptimizer_blacklist.hpp, and in add_src /
serialization folder. Also be warn, some common files
(parameters_simulation.hpp, simulation/crustcrawler_arm.hpp and simulation /
nvironment_arm.hpp) are exactly the same except for initialization in
structures, that is done with constexpr expression despite of const expression.


# Target
The target is not dynamic, that means that if you change
Params::dims::dim_objective. Many spots in the code are marked with the
"DYNAMIZE" mention, they have to be changed to make the target dynamic. For
now, it is simply a 3 dimensional vector, corresponding to a position. That
means that, whatever the dimension of the map, the estimation will be done
only on the three physical dimensions (x, y, z)


# Simulation
The Params::global::mass and Params::global::rotation parameters in main.cpp
can be switched to launch limbo in a different simulation. The
Params::global::coeff works exactly the same way that map_elite/modif_archive.


# Real
For launching limbo on the real arm, you have to change Params::global::real in
main.cpp.  
For each iteration, the program will give you a controller and will ask for the
new position of the cube (has to type "x y z" on console). You have to launch
the controller provided in an other terminal, with the
map_elite/crustcrawler_arm code.