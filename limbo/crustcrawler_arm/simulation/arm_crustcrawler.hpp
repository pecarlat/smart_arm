#ifndef ROBOT_ARM_HORI_HPP
#define ROBOT_ARM_HORI_HPP

#include <robot/robot.hh>
#include "environment_arm.hpp"

namespace robot {
	
	struct Params {
		
		// Plinth
		static constexpr double	body_width = 0.117;
		static constexpr double	body_height = 0.05;
		static constexpr double	body_length = 0.117;
		static constexpr double	body_mass = 20;
		
		// Pivot
		static constexpr double	p1_width = 0.1;
		static constexpr double	p1_height = 0.056;
		static constexpr double	p1_length = 0.1;
		static constexpr double	p1_mass = 0.2;
		
		// Arm
		static constexpr double	p2_width = 0.1;
		static constexpr double	p2_height = 0.23;
		static constexpr double	p2_length = 0.032;
		static constexpr double	p2_mass = 0.2;
		
		static constexpr double	p3_width = 0.051;
		static constexpr double	p3_height = 0.155;
		static constexpr double	p3_length = 0.032;
		static constexpr double	p3_mass = 0.2;
		
		static constexpr double	p4_width = 0.051;
		static constexpr double	p4_height = 0.08;
		static constexpr double	p4_length = 0.032;
		static constexpr double	p4_mass = 0.2;
		
		static constexpr double	p5_width = 0.051;
		static constexpr double	p5_height = 0.07;
		static constexpr double	p5_length = 0.032;
		static constexpr double	p5_mass = 0.2;
		
		// Second pivot (wrist)
		static constexpr double	p6_width = 0.021;
		static constexpr double	p6_height = 0.021;
		static constexpr double	p6_length = 0.021;
		static constexpr double	p6_mass = 0.2;
		
		// Fingers
		static constexpr double finger_height = 0.171;
		static constexpr double	finger_inter_dist = 0.055;
		
		static constexpr double	finger_width1 = 0.085;
		static constexpr double	finger_height1 = 0.041;
		static constexpr double	finger_length1 = 0.035;
		static constexpr double	finger_mass1 = 0.016;
		static constexpr double	finger_width2 = 0.015;
		static constexpr double	finger_height2 = 0.025;
		static constexpr double	finger_length2 = 0.035;
		static constexpr double	finger_mass2 = 0.016;
		static constexpr double	finger_width3 = 0.002;
		static constexpr double	finger_height3 = 0.02;
		static constexpr double	finger_length3 = 0.034;
		static constexpr double	finger_mass3 = 0.016;
		static constexpr double	finger_width4 = 0.002;
		static constexpr double	finger_height4 = 0.05;
		static constexpr double	finger_length4 = 0.022;
		static constexpr double	finger_mass4 = 0.016;
		static constexpr double	finger_width5 = 0.002;
		static constexpr double	finger_height5 = 0.02;
		static constexpr double	finger_length5 = 0.022;
		static constexpr double	finger_mass5 = 0.016;
	};


	class Arm : public Robot {
	
	public:
	
		Arm(ode::Environment_arm & env, float height);
		~Arm();
		
		
		/**
		 * @brief get the position of the end of the gripper
		 * @return the position
		 */
		Eigen::Vector3d pos() const;
		
		/**
		 * @brief add a segment to the arm
		 * @param i is the index of the segment in the arm
		 * @param the mass of the segment
		 * @param the length of the segment
		 * @param the width of the segment
		 * @param the height of the segment
		 * @param represents the height where to place the segment (the height of the object i-1 of the arm)
		 * @param the environment where the arm is located
		 * @param a parameter indicating the orientation of the servo-motor : 0 is a rotation on x,y plan and 1 a rotation on y,z plan 
		 * @param the environment where the arm is located
		 */
		void add_seg(int i, float mass, float length, float width, float height, float current_height, ode::Environment_arm& env, int dim, int servo_type = 0);
		
		/**
		 * @brief Add a finger to the arm
		 * @param i is the index of the finger in the arm
		 * @param the mass of all the parts of the finger
		 * @param the length of all the parts of the finger
		 * @param the width of all the parts of the finger
		 * @param the height of all the parts of the finger
		 * @param represents the height where to place the finger (the height of the object i-1 of the arm)
		 * @param the environment where the arm is located
		 * @param the distance between the two fingers
		 * @param a boolean : true if did the right finger, false for the left finger
		 */
		void add_finger(int i, float mass[5], float length[5], float width[5], float height[5], float current_height, ode::Environment_arm& env, float inter_dist, bool right);
		
		
		// Getters / Setters
		void setHeight(float height) { _height = height; }
		float getHeight() { return _height; }
		
	 
	private:
	
		float _height;
	};
}
#endif
