// THIS IS A GENERATED FILE - DO NOT EDIT
#define ROTATION
#define BL
#line 1 "/home/pecarlat/dream/limbo-gp_multi/exp/crustcrawler_arm/main.cpp"
//#define SHOW_TIMER
#include <limbo/limbo.hpp>
#include <limbo/inner_cmaes.hpp>
#include <limbo/gp_auto_mean.hpp>
#include "limbo/parallel.hpp"

#include "add_src/gp_blacklist.hpp"
#include "add_src/gp_auto_blacklist.hpp"
#include "add_src/gp_auto_mean_blacklist.hpp"
#include "add_src/exhaustiveSearchMap.hpp"
#include "add_src/meanMap.hpp"
#include "add_src/ucb_multi.hpp"
#include "add_src/stop_tolerance.hpp"
#include "add_src/random_init.hpp"

#include "serialization/statArchive.hpp"
#include "serialization/statTransferts.hpp"
#include "serialization/statObservations.hpp"
#include "serialization/statHParams.hpp"

#include "simu.hpp"
#include "arm_controller.hpp"
#include "parameters_simulation.hpp"
#include "boptimizer_blacklist.hpp"

#include "tbb/tbb.h"

#ifdef GRAPHIC
#define NO_PARALLEL
#include "renderer/osg_visitor.hh"
#endif


using namespace limbo;

struct Params {
	// Global parameters
	struct global {
		static constexpr int map_dim = 3; // Number of dimension of the map
		static constexpr int waypoint_size = 7; // size of one waypoint
		static constexpr bool real = true; // If true, limbo has to be launched in the real robot
		static constexpr bool mass = false; // If true, the new archive will be with a new mass for the cube
		static constexpr bool rotation = true; // If true, the new archive will be with a new rotation for the cube
		static constexpr float coeff = 1.0; // The coefficient for the mass or for the rotation of the cube
	};
	struct dims {
		static constexpr int dim_objective = 3; // objective to reach (here, we try to find a controler that reach a 3d position)
	};
	
	struct boptimizer {
		BO_PARAM(double, noise, 0.01); // Data given by limbo are noised
		BO_PARAM(int, dump_period, 1); // Frequency for recording data
	};
	struct maxiterations {
		BO_PARAM(int, n_iterations, 100); // Number max of iterations for Limbo
	};
	struct mintolerance {
		BO_PARAM(double, tolerance, -0.1); // Distance max accepted between observation and target
	};
	struct init {
		BO_PARAM(int, nb_samples, 10); // Samples for the initialization
	};
	struct ucb {
		BO_PARAM(double, alpha, 0.104); // Parameter for exploration : bigger it is, more it will explore
	};
	struct cmaes : public defaults::cmaes {
		
	};
	struct gp_auto {
		BO_PARAM(int, n_rprop, 300); // ?
		BO_PARAM(int, rprop_restart, 10); // ?
	};
	struct gp_auto_mean {
		BO_PARAM(int, n_rprop, 300); // ?
		BO_PARAM(int, rprop_restart, 10); // ?
	};
	struct target {
		// Dynamize (vectorXd(dims::dim_objective))
		static Eigen::Vector3d point; // Target to reach
	};
	// Structure used to store the archive
	struct archiveparams {
		struct elem_archive {
			std::vector<float> object_state;
			float fit;
			Arm_controller controller;
		};
		struct classcomp { // Compare both keys
			bool operator() (const std::vector<float>& lhs, const std::vector<float>& rhs) const {
				assert(lhs.size() == rhs.size());
				int i(0);
				int precision(10000);
				while(i < lhs.size()-1 && round(lhs[i]*precision) == round(rhs[i]*precision))
					i++;
				return round(lhs[i]*precision) < round(rhs[i]*precision);
			}
		};
		typedef std::map<std::vector<float>, elem_archive, classcomp> archive_t;
		static archive_t archive;
	};
};




// Some informations about the simulation (borders of the grid)
Data d;

// Get the controller corresponding (8 + 1 values) to a vector/waypoint (7 values)
Arm_controller getControllerFromVector(std::vector<float> v) {
	assert(v.size()%Params::global::waypoint_size == 0);
	Arm_controller ac;
	std::vector<float> tmp;
	ac.setOrigin(0.5f);
	for(int i(0) ; i < v.size()/Params::global::waypoint_size ; i++) {
		tmp.clear();
		for(int j(0) ; j < Params::global::waypoint_size ; j++)
			tmp.push_back(v[(i*Params::global::waypoint_size)+j]);
		ac.addWaypoint(tmp);
	}
	return ac;
}

namespace global {
	struct timeval timev_selection;	// Initial absolute time (static)	
	std::string res_dir;
};

// Fitness function
template<typename Params, int obs_size = 1, bool exept = true>
struct fit_eval_map {
	BOOST_STATIC_CONSTEXPR int dim_in = Params::global::map_dim;
	BOOST_STATIC_CONSTEXPR int dim_objective = obs_size;
	
	fit_eval_map() {
		timerclear(&global::timev_selection);
		gettimeofday(&global::timev_selection, NULL);
	}
	
	Eigen::VectorXd operator()(Eigen::VectorXd x) const {
		std::vector<float> key;
		for(int i(0) ; i < x.size() ; i++)
			key.push_back(x[i]);
		
		if(Params::archiveparams::archive.count(key) == 0) {
			throw 42;
		}
		
		// Put them into our controller
		Arm_controller ac;
		ac = Params::archiveparams::archive.at(key).controller;
		
		// Dynamize
		Eigen::Vector3d now;
		if(!Params::global::real) {
			Simu simu(d);
			try {
				simu.launch(ac);
			} catch(int e) {
				simu.setCrash(true);
			}
			
			// Dynamize
			if(!simu.getCrash() && obs_size == Params::dims::dim_objective) {
				now = simu.getEnv()->getCubePos(); // Absolute position of the cube after experiment
				now[0] = (now[0] - d.getBorder_left_x()) / Params_Simu::grid::size_x;
				now[1] = (now[1] - d.getBorder_left_y()) / Params_Simu::grid::size_y;
				now[2] = (now[2] - d.getBorder_left_z()) / Params_Simu::grid::size_z;
			} else {
				throw 42; // Blacklist
			}
		} else {
			std::cout << "Have to launch this controller :" << std::endl;
			ac.showRealController();
			std::cout << "Where is the cube ? (x, then y, then z)" << std::endl;
			std::cin >> now[0];
			std::cin >> now[1];
			std::cin >> now[2];
			if(now[0] == -1 && now[1] == -1 && now[2] == -1)
				throw 42;
			now[0] = (now[0] - d.getBorder_left_x()) / Params_Simu::grid::size_x;
			now[1] = (now[1] - d.getBorder_left_y()) / Params_Simu::grid::size_y;
			now[2] = (now[2] - d.getBorder_left_z()) / Params_Simu::grid::size_z;
		}
		
		
		return now;
	}
};

// Load the archive and store it on a Params::archiveparams::archive_t
Params::archiveparams::archive_t load_archive(std::string archive_name) {
	Params::archiveparams::archive_t archive;
	std::ifstream monFlux(archive_name.c_str());
	
	std::string line;
	if(monFlux) {
		while(std::getline(monFlux, line)) {
			Params::archiveparams::elem_archive elem;
			std::vector<float> candidate(Params::global::map_dim);
			std::vector<float> cntrller;
			std::istringstream iss(line);
			
			int i(0);
			float data;
			while(iss >> data) {
				if(i < Params::global::map_dim) {
					candidate[i] = data;
					elem.object_state.push_back(data);
				}
				if(i == Params::global::map_dim) elem.fit = data;
				if(i > Params::global::map_dim) cntrller.push_back(data);
				i++;
			}
			
			// Put them into our controller
			elem.controller = getControllerFromVector(cntrller);
			
			archive[candidate] = elem;
		}
		assert(!archive.empty());
	} else {
		std::cout << "Cannot read the archive" << std::endl;
		exit(-1);
	}
	
	return archive;
}

// Help text
void printHelp() {
	std::cout << "Expected format :" << std::endl;
	std::cout << "\tcmd archive_xxxx.dat -> optimization launch with random target" << std::endl;
	std::cout << "\tcmd archive_xxxx.dat t1 t2 t2 ... -> launch a optimization focused on a defined target" << std::endl;
	std::cout << "\tcmd archive_xxxx.dat -k k1 k2 k3 ... -> check if key belongs to archive_xxxx.dat" << std::endl;
	std::cout << "\tcmd -s wp1 wp2 wp3 ... -> launch the simulation of a controller (variable number of waypoints allowed)" << std::endl;
	std::cout << "\tcmd -h -> prints this" << std::endl;
}


// Dynamize
Eigen::Vector3d Params::target::point;
Params::archiveparams::archive_t Params::archiveparams::archive;
//BO_DECLARE_DYN_PARAM(Eigen::VectorXd, Params::meanconstant, constant);

int main(int argc, char** argv) {
	
	srand(time(NULL));
	if(Params::global::mass)
		d.setMass_coeff(Params::global::coeff);
	else if(Params::global::rotation)
		d.setRotation_coeff(Params::global::coeff);
	
	if(argc < 2) {
		printHelp();
		return -1;
	}
	
	Params::archiveparams::archive = load_archive(argv[1]);
	std::cout << "[initialization] " << Params::archiveparams::archive.size() << " elements loaded" << std::endl;
	
	
	/* cmd archive_xxxx.dat
	 * -> random target
	 * /!\ Warn : targets don't have the same difficulty 
	 */
	if(argc == 2) {
		if(strcmp(argv[1], "-h") == 0) {
			printHelp();
			return 0;
		} else {
			Eigen::VectorXd temp = Eigen::VectorXd::Random(Params::dims::dim_objective, 1);
			// Dynamize
			Params::target::point = Eigen::Vector3d(fabs(temp(0)), fabs(temp(1)), fabs(temp(2)));
		}
	} else {
		
		/* cmd -s controller
		 * -> launch a controller in simulation ad print final position of the cube
		 */
		if(strcmp(argv[1], "-s") == 0) {
			if((argc-2)%Params::global::waypoint_size != 0) {
				std::cout << "Unvalid controller" << std::endl;
				return -1;
			}
			
			std::vector<float> vect;
			for(int i(2) ; i < argc ; i++)
				vect.push_back(atof(argv[i]));
			Arm_controller ac(getControllerFromVector(vect));
			ac.showControllers();
			std::cout << std::endl;
			
			dInitODE();
	
			Simu simu(d);
			try {
				simu.launch(ac);
			} catch(int e) {
				simu.setCrash(true);
			}
			
			if(!simu.getCrash()) {
				// Dynamize
				Eigen::Vector3d now(simu.getEnv()->getCubePos()); // Absolute position of the cube after experiment
				now[0] = (now[0] - d.getBorder_left_x()) / Params_Simu::grid::size_x;
				now[1] = (now[1] - d.getBorder_left_y()) / Params_Simu::grid::size_y;
				now[2] = (now[2] - d.getBorder_left_z()) / Params_Simu::grid::size_z;
		
				std::cout << "final pos : " << now[0] << " / " << now[1] << " / " << now[2] << std::endl;
			} else {
				std::cout << "crash... :(" << std::endl;
			}
	
			dCloseODE();
	
			return 0;
		}
		
		/* cmd archive_xxxx.dat k1 k2 k3 k4 k5 k6 (for 6 dimensional map)
		 * -> give information about a key (controller associated)
		 */
		else if(strcmp(argv[2], "-k") == 0) {
			if((argc-3)%Params::global::map_dim != 0) {
				std::cout << "Unvalid controller" << std::endl;
				return -1;
			}
			
			std::vector<float> tmp;
			for(int i(0) ; i < Params::global::map_dim ; i++)
				tmp.push_back(atof(argv[i+3]));
		
			if(Params::archiveparams::archive.count(tmp) == 0) {
				std::cout << "No cell corresponding to this key on the map" << std::endl;
			} else {
				Params::archiveparams::archive.at(tmp).controller.showRealController();
				std::cout << std::endl;
			}
			return 0;
		}
		
		/* cmd archive_xxxx.dat t1 t2 t3
		 * -> given target
		 */
		else if(argc = 2+Params::dims::dim_objective) {
			// Dynamize
			Eigen::Vector3d temp(atof(argv[2]), atof(argv[3]), atof(argv[4]));
			temp = temp.transpose();
			Params::target::point = temp;
		} else {
			printHelp();
			return -1;
		}
	}
	
	std::cout << "[initialization] target is : ";
	std::cout << Params::target::point.transpose() << std::endl << std::endl;
	
	
	
	// Dynamize ?
	typedef kernel_functions::SquaredExpARD<Params> Kernel_t;
	typedef init_functions::RandomSamplingArchiveBL<Params> Init_t;
	typedef boost::fusion::vector< stopping_criterion::MaxIterations<Params>, stopping_criterion::MinTolerance<Params>, stopping_criterion::DataConfluence<Params> > Stop_t;
	
	
	/* Version without mean (without global adaptation of the estimation map)
	 * To switch to, uncomment this part and comment the following one
	 * You'll also have to modify serialization/statHParams.hpp by commenting incated parts
	 */
	/*
	typedef mean_functions::MeanArchive_Map<Params>  Mean_prior_t;
	typedef model::GPAutoBlackList<Params, Kernel_t, Mean_prior_t> GP_obs_prior_t;
	typedef acquisition_functions::UCB_multi<Params, GP_obs_prior_t> Acqui_obs_prior_t;
	typedef inner_optimization::ExhaustiveSearchArchive<Params> InnerOpt_obs_prior_t;
	typedef boost::fusion::vector< stat::Acquisitions<Params>, stat::StatObservations<Params> > Stat_prior_t;
	
	typedef BOptimizerBL<	Params,
							model_fun		<GP_obs_prior_t>, 
							init_fun		<Init_t>, 
							acq_fun			<Acqui_obs_prior_t>, 
							inneropt_fun	<InnerOpt_obs_prior_t>, 
							stat_fun		<Stat_prior_t>, 
							stop_fun		<Stop_t> 					>
				opt_obs_prior_mean_t;
	*/
	
	/* Version with mean (with global adaptation of the estimation map)
	 * To switch to, uncomment this part and comment the antecedent one
	 * You'll also have to modify serialization/statHParams.hpp by uncommenting incated parts
	 */
	///*
	typedef mean_functions::MeanFunctionARD<Params, mean_functions::MeanArchive_Map<Params> > Mean_prior_mean_t;
	typedef model::GPAutoMeanBlackList<Params, Kernel_t, Mean_prior_mean_t> GP_obs_prior_mean_t;
	typedef acquisition_functions::UCB_multi<Params, GP_obs_prior_mean_t> Acqui_obs_prior_mean_t;
	typedef inner_optimization::ExhaustiveSearchArchive<Params> InnerOpt_obs_prior_mean_t;
	typedef boost::fusion::vector< stat::Acquisitions<Params>, stat::StatObservations<Params>, stat::StatHParamsMean<Params> > Stat_prior_mean_t;
	
	typedef BOptimizerBL<	Params,
							model_fun		<GP_obs_prior_mean_t>, 
							init_fun		<Init_t>, 
							acq_fun			<Acqui_obs_prior_mean_t>, 
							inneropt_fun	<InnerOpt_obs_prior_mean_t>, 
							stat_fun		<Stat_prior_mean_t>, 
							stop_fun		<Stop_t> 					>
				opt_obs_prior_mean_t;
	//*/
	
	
	dInitODE();
	
	opt_obs_prior_mean_t opt_obs_prior_mean_tr;
	opt_obs_prior_mean_tr.optimize(fit_eval_map<Params, Params::dims::dim_objective>(), false);
	
	dCloseODE();
	
	
	// Results
	std::vector<float> best_key;
	try {
		for(int i(0) ; i < opt_obs_prior_mean_tr.best_sample().size() ; i++)
			best_key.push_back(opt_obs_prior_mean_tr.best_sample()[i]);
		std::cout << std::endl << "Best : " << opt_obs_prior_mean_tr.best_sample().transpose() << std::endl;
		std::cout << "Corresponds to controller : " << std::endl;
		Params::archiveparams::archive.at(best_key).controller.showRealController();
		std::cout << std::endl;
	} catch(...) {
		std::cout << std::endl << "No controller corresponds to this target" << std::endl;
	}

	return 0;
}


