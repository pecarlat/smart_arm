#ifndef STAT_ARCHIVE_HPP_
#define STAT_ARCHIVE_HPP_


namespace limbo {
	namespace stat {
		template<typename Params>
		struct StatArchive : public Stat<Params> {
			StatArchive() {
				
			}
			
			template<typename BO>
			void operator()(const BO& bo) {
				if (!bo.dump_enabled())
					return;
			
			std::ostringstream oss;
			oss << bo.iteration();
			
			std::string fname("archive_");
			fname += oss.str();
			fname += ".dat";
			
			std::ofstream ofs((bo.res_dir() + "/" + fname).c_str());
			
			for(typename Params::archiveparams::archive_t::iterator it = Params::archiveparams::archive.begin() ; it != Params::archiveparams::archive.end() ; it++) {
				Eigen::VectorXd v(it->first.size());
				
				for(int i(0) ; i < it->first.size() ; i++) {
					ofs << it->first[i] << " ";
					v[i] = it->first[i];
				}
				
				if(bo.samples().size() == 0) {
					ofs << (bo.meanfunction()(v));
					return;
				}
				
				// compute
				Eigen::VectorXd k(bo.samples().size());
				for(int i(0) ; i < k.size() ; i++)
					k[i] = bo.kernelfunction()(bo.samples()[i], v);

				Eigen::VectorXd mean_vector(bo.samples().size());
				for(int i(0) ; i < mean_vector.size() ; i++)
					mean_vector[i] = bo.meanfunction()(bo.samples()[i]);
				
				ofs << bo.meanfunction()(v) + (k.transpose()*bo.inverted_kernel()*(bo.observations()-mean_vector))[0] << std::endl;
			}

			}
		};
	}
}
#endif
