#ifndef STAT_OBSERVATIONS_HPP_
#define STAT_OBSERVATIONS_HPP_


namespace limbo {
	namespace stat {
		
		double dist_polaire(Eigen::Vector2d i,Eigen::Vector2d j) {
			return sqrt(i(0)*i(0) + j(0)*j(0) -2*i(0)*j(0)*cos(i(1)-j(1))); 
		}
		
		template<typename Params>
		bool compareVectorXd(Eigen::VectorXd i, Eigen::VectorXd j) {
			return -((Params::target::point-i).norm()) < -((Params::target::point-j).norm());
		}
		
		template<typename Params>
		bool compareVectorXd_polaire(Eigen::VectorXd i, Eigen::VectorXd j) {
			return -dist_polaire(i, Params::target::point) < -dist_polaire(j, Params::target::point);
		}
		
		template<typename Params>
		struct StatObservations : public Stat<Params> {
			
			std::ofstream _ofs;
			
			StatObservations() {

			}

			template<typename BO>
			void operator()(const BO& bo) {
				this->_create_log_file(bo, "observations.dat");	
				if (!bo.dump_enabled())
					return;
				
				(*this->_log_file) << bo.iteration() << "	";
				if(bo.observations()[0].size() > 1) {
					(*this->_log_file) << bo.observations()[bo.observations().size()-1].transpose() << " " 
										<< -(Params::target::point- bo.observations()[bo.observations().size()-1]).norm() << "	 " 
										<< -(Params::target::point- *std::max_element(bo.observations().begin(), 
																						bo.observations().end(), 
																						compareVectorXd<Params>)).norm()
										<<std::endl;
				} else {
					(*this->_log_file) << bo.observations()[bo.observations().size()-1] << "	 " << bo.best_observation() << std::endl;
				}
			}
		};
		
		template<typename Params>
		struct StatObservations_polaire : public Stat<Params> {
			std::ofstream _ofs;
			StatObservations_polaire(){

			}

			template<typename BO>
			void operator()(const BO& bo) {
				this->_create_log_file(bo, "observations.dat");	
				if (!bo.dump_enabled())
					return;
				
				(*this->_log_file) << bo.iteration() << "	";
				(*this->_log_file) << bo.observations()[bo.observations().size()-1].transpose() << " " 
									<< -dist_polaire(Params::target::point, bo.observations()[bo.observations().size()-1]) << "	 " 
									<< -dist_polaire(Params::target::point, *std::max_element(bo.observations().begin(), 
																								bo.observations().end(), 
																								compareVectorXd_polaire<Params>)) 
									<< std::endl;
				
				std::cout << "current obs: " << -dist_polaire(Params::target::point, bo.observations()[bo.observations().size()-1]) 
							<< " best Obs: " << -dist_polaire(Params::target::point, *std::max_element(bo.observations().begin(), 
																										bo.observations().end(), 
																										compareVectorXd_polaire<Params>)) 
								<< std::endl;
			}
		};
	}
}
#endif
