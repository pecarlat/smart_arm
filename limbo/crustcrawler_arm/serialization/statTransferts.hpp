#ifndef STAT_TRANSFERTS_HPP_
#define STAT_TRANSFERTS_HPP_


namespace limbo
{
  namespace stat
  {

    template<typename Params>
    struct StatTransferts : public Stat<Params>
    {
      std::ofstream _ofs;
      StatTransferts(){

      }

      template<typename BO>
      void operator()(const BO& bo)
      {
	this->_create_log_file(bo, "transferts.dat");	
	if (!bo.dump_enabled())
	  return;
      }
    };
  }
}
#endif
