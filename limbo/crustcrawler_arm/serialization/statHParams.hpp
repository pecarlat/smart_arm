#ifndef STAT_HPARAMS_HPP_
#define STAT_HPARAMS_HPP_


namespace limbo
{
  namespace stat
  {
	// Stats concerning the H parameter = the coefficient applied to each dimension of the map used to get the estimation
    template<typename Params>
    struct StatHParamsMean : public Stat<Params>
    {
      std::ofstream _ofs;
      StatHParamsMean(){

      }

      template<typename BO>
      void operator()(const BO& bo)
      {
	this->_create_log_file(bo, "hparams.dat");	
	if (!bo.dump_enabled())
	  return;

	/* If not mean, comment the following line
	 */
	(*this->_log_file)<<bo.iteration()<<"\t"<<bo.model().kernel_function().h_params().transpose()<< "\t \t"<<bo.model().mean_function().h_params().transpose()<<"\t \t"<<bo.model().get_lik() <<std::endl;


      }
    };
    template<typename Params>
    struct StatHParams : public Stat<Params>
    {
      std::ofstream _ofs;
      StatHParams(){

      }

      template<typename BO>
      void operator()(const BO& bo)
      {
	this->_create_log_file(bo, "hparams.dat");	
	if (!bo.dump_enabled())
	  return;

	/* If not mean, comment the following line
	 */
	(*this->_log_file)<<bo.iteration()<<"\t"<<bo.model().kernel_function().h_params().transpose()<< "\t \t"<<bo.model().get_lik() <<std::endl;


      }
    };
  }
}
#endif
