# Map-Elite generation
We want to generate a diversity of arm trajectories of interest for object
manipulation while exploring an environment. To that, we utilize an
evolutionary algorithm called Multi-dimensional Archive of Phenotypic Elites
(MAP-Elites). This algorithm defines a multi-dimensional grid (called map) that
it will try to fill. Each dimension of the grid corresponds to a behaviour
descriptor. The dimensions are discretized and each ’cell’ in this map contains
at most one controller (e.g., a trajectory): the controller that generates a
behaviour fitting with the behaviour descriptor value of the cell and having
the best performance according to a user defined fitness function. Controllers
are randomly generated at first. They are then evaluated to see where they fit
in the map. If the corresponding cell is empty, they are added to the map;
otherwise, only the best performing controllers among those in the cells and
the new ones are kept in the map.


# Installation
This installation procedure was tested on Ubuntu 14.04.2, just after its
installation, with no other softwares. It will need the installation pack
(installation_files.tar.gz).

```
sudo apt-get install libboost-dev libboost-test-dev libboost-filesystem-dev libboost-program-options-dev libboost-graph-parallel-dev python g++ libtbb-dev libeigen2-dev python-simplejson libgoogle-perftools-dev libode-dev libopenscenegraph-dev
mkdir dream && cd dream
git clone https://github.com/jbmouret/sferes2.git
git clone https://github.com/jbmouret/robdyn.git
git clone https://github.com/jbmouret/libdynamixel.git

echo robdyn >> sferes2/modules.conf
echo dynamixel >> sferes2/modules.conf
```

Put include and lib folders of installation pack in dream/  
Replace dream/sferes2/modules folder by modules folder situated in
installation pack  
Place bin folder of installation pack on your home folder (home/$USER/)

```
cp -r robdyn/data/ sferes2/
```
Add osgGA at the line 160 of sferes2/wscript (has to become "['osg', 'osgGA',
'osgDB', 'osgUtil',")  
Add at the end of the file ~/.bashrc the two following lines:
```
export LD_LIBRARY_PATH=/home/$USER/dream/lib:$LD_LIBRARY_PATH
export PATH=/home/$USER/bin:$PATH
```
Then source ~/.bashrc


# Configuration
```
cd robdyn
./waf configure --boost-libs /home/$USER/dream/lib/ --boost-include /home/$USER/dream/include/ --eigen /home/$USER/dream/include --prefix /home/$USER/dream/
./waf build
cd ../libdynamixel
./waf configure --boost-libs /home/$USER/dream/lib/ --boost-include /home/$USER/dream/include/
cd ../sferes2/
./waf configure --boost-libs /home/$USER/dream/lib/ --boost-include /home/$USER/dream/include/ --eigen /home/$USER/dream/include/ --robdyn /home/$USER/dream/ --robdyn-osg yes --dynamixel /home/$USER/dream/ 
./waf build
```

Put map_elite/crustcrawler_arm in sferes2/exp/  
Put map_elite/evo_loat_mutable.hpp and float_mutable.hpp in sferes/gen/


# Launch
In dream/sferes2/ folder
```
./waf --exp crustcrawler_arm
build/default/exp/crustcrawler_arm/main_graphic 0.5 0.5 0.5 0.5 0.5 0.5 0.5
```
Launch the controller 0.5 0.5 ..
```
build/default/exp/crustcrawler_arm/main_text -> Launch Map-Elite
```


# Simulation
The evaluated scenario is composed of a table, a small cube and four baskets.
The goal is to discover what the robot can do with the small cube and in
particular what displacement of the object it can perform, without providing it
with pushing or grasping primitives. This simulation can easily be changed, it
is mostly dynamic.  
The simulated robot arm is based on the features of a Crustcrawler Pro-Series
robotic arm.  
In order to change the simulation:
- change the arm dimensions : all values are in
simulation/crustcrawler_arm.hpp, in meters / kilograms
- change the environment : main values are in parameters_simulation.hpp and
calls of environment objets are done in the head of simu.cpp


# The controller
A controller represents a trajectory, initially composed of one end position
of the arm. A position is defined by seven values related to the six joint
values composing the arm, and another value related to the opening of the
gripper in between positions.  
In order to change the controller:
- it is not really dynamic.. The main code to change is in
arm_controller.cpp/.hpp, but be warn, it is probably not enough and it
probably exists some pieces of codes with non-dynamics references


# Behavioral descriptors
For now, three different behaviour descriptors are implemented in our code:
- The first one is 3-dimensional : x, y, z, and corresponds to the final
position of the cube after the move
- The second one is 5-dimensional : x, y, z, a, w and corresponds to the final
position of the cube after the move, and to the effector state when cube moves
for the first time (a is the angle of the approach and w the rotation of the
wrist)
- The third one is 6-dimensional : y, y, z, ∆x, ∆y and ∆z; where x, y and z are
the final position of the cube the movement, and ∆x, ∆y and ∆z are, for each
dimension, the difference between the two most extreme positions of the cube
during its movement (∆x = max(x) − min(x)).  
In order to change the map :
- It can be changed in main.cpp, by changing the variable
Params::global::map_dim and by changing the corresponding include in headers.


# The performance - locale competition
If a controller corresponds to a cell of the map that already has been filled,
only the 'best' controller is conserved. They are juged according to a
performance function. Few extensions of this function are implemented for now
(variance of main joints of the arm, torques of all motors, with or without
punition when touches the table...).  
In order to change the performance :
- The performance function can be changed in the file
parameters_simulation.hpp, by changing some values (as indicated in comments)


# The real arm
To launch a controller on the real arm, you have to uncomment the paragraph at
the end of the main in main.cpp.


# Cluster
The dynamixel library is not installed on the cluster, so you have to delete
everything concerning the real arm when you launch a process (rm -r
exp/crustcrawler_arm/real*)