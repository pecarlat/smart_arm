#include <iostream>
#include <limits>
#include "robotArm.hpp"

#include <fstream>


RobotArm::RobotArm() {
	setCrash(false);
	setErrors(0);
	init();
}

RobotArm::~RobotArm() {
	relax();
}

void RobotArm::init() {
	try {
		getController().open_serial(DYNAMIXELSERIAL, B1000000);
		
		// Scan actuators IDs
		getController().scan_ax12s();
		
		const std::vector<byte_t>& ax12_ids = getController().ax12_ids();
		if (!ax12_ids.size()) {
			std::cerr << "[real_arm] no dynamixel detected" << std::endl;
			setCrash(true);
			return;
		}
		std::cout << "[real_arm] " << ax12_ids.size() << " dynamixel are connected" << std::endl;
		
		getActuatorsIds().push_back(1); //turntable : MX-28
		getActuatorsIds().push_back(2); // actuators 2 & 3 : 1st joint : 2*MX-106
		getActuatorsIds().push_back(3);
		getActuatorsIds().push_back(4); // 2nd joint : MX-106
		getActuatorsIds().push_back(5); // 3rd joint : MX-64
		getActuatorsIds().push_back(6); // 4th joint : MX-28
		getActuatorsIds().push_back(7); // 5th joint "base gripper" : MX-28
		getActuatorsIds().push_back(8); // right finger joint : AX-18A
		getActuatorsIds().push_back(9); // left finger joint : AX-18A
		
		// PID
		/* P coefficient ; cw compliance slope
		 * I coefficient ; ccw compliance margin
		 * D coefficient ; cw compliance margin
		 */
		changePValue(8);
		changeIValue(0);
		changeDValue(0);
	}
	catch (dynamixel::Error e) {
		std::cerr << "[real_arm] error (dynamixel): " << e.msg() << std::endl;
	}
}

void RobotArm::reset() {
	try {
		if(!getController().isOpen()) {
			std::cout << "[real_arm] re-opening dynamixel's serial" << std::endl;
			getController().open_serial(DYNAMIXELSERIAL, B1000000);
		}
		getController().flush();
	}
	catch (dynamixel::Error e) {
		std::cerr << "[real_arm] error (dynamixel): " << e.msg() << std::endl;
		setCrash(true);
		return;
	}

	enable();
}

void RobotArm::relax() {
	try {
		for (size_t i(0) ; i < getActuatorsIds().size() ; ++i) {
			getController().send(dynamixel::ax12::TorqueEnable(getActuatorsIds()[i], false));
			getController().recv(Parameters::read_duration, getStatus());
		}
	} catch(dynamixel::Error e) {
		std::cerr << "[real_arm] dynamixel error : " << e.msg() << std::endl;
	}
}

void RobotArm::enable() {
	try {
		std::cout << "[real_arm] enable motor..." << std::endl;
		for(size_t i(0) ; i < getActuatorsIds().size() ; ++i) {
			getController().send(dynamixel::ax12::TorqueEnable(getActuatorsIds()[i], true));
			getController().recv(Parameters::read_duration, getStatus());
		}
		
		usleep(1e5);
	} catch(dynamixel::Error e) {
		std::cerr << "[real_arm] dynamixel error : " << e.msg() << std::endl;
	}
}

std::vector<float> RobotArm::get_joint_values(std::vector<byte_t> actuators_ids) {
	std::vector<float> current_pos;
	std::vector<int> current_pos_step_per_turn;
	if(actuators_ids.empty())
		actuators_ids = getActuatorsIds();

	for (size_t i(0) ; i < actuators_ids.size() ; i++) {
		try {
			getController().send(dynamixel::ax12::GetPosition(actuators_ids[i]));
			getController().recv(Parameters::read_duration, getStatus());
			current_pos_step_per_turn.push_back(getStatus().decode16());

			if(actuators_ids[i] < 8 && actuators_ids[i] != 2)
				current_pos.push_back(MX_stepperturn_to_rad(current_pos_step_per_turn[i]));
			else if(actuators_ids[i] >= 8)
				current_pos.push_back(AX_stepperturn_to_rad(current_pos_step_per_turn[i]));
		} catch(dynamixel::Error e) {
			std::cerr << "[real_arm] get joint values : error :" << e.msg() << std::endl;
			return std::vector<float>(1, 100);
		}
	}
		usleep(1e3);

	return current_pos;
}

bool RobotArm::set_joint_values(std::vector<float> values, std::vector<byte_t> actuators_ids) {
	if(actuators_ids.empty())
		actuators_ids = getActuatorsIds();
	try {
		std::vector<int> pos(actuators_ids.size());

		int i(0);
		for(std::vector<float>::iterator it = values.begin(); it != values.end(); it++) {
			if(actuators_ids[i] < 8) {
				pos[i] = rad_to_stepperturn_MX(*it);
				if(actuators_ids[i] == 2) {
					pos[i+1] = pos[i];
					it = values.insert(it,pos[i]);
					i++;
					it++;
				}
			} else {
				pos[i]=rad_to_stepperturn_AX(*it);
			}
			i++;
		}

		getController().send(dynamixel::ax12::SetPositions(actuators_ids, pos));
		getController().recv(Parameters::read_duration, getStatus());
		
		usleep(1e4);

		return true;

	} catch(dynamixel::Error e) {
		std::cerr << "[real_arm] set_joint_values : dynamixel error : " << e.msg() << std::endl;
		return false;
	}
}

void RobotArm::changePValue(int val, int servo_index) {
	if(servo_index == -1) {
		for(int i(2) ; i < 7 ; i++) {
			getController().send(dynamixel::ax12::WriteData(i, dynamixel::ax12::ctrl::cw_compliance_slope, val));
			getController().recv(Parameters::read_duration, getStatus());
		}
	} else if(servo_index >= 2 && servo_index <= 6) {
		getController().send(dynamixel::ax12::WriteData(servo_index, dynamixel::ax12::ctrl::cw_compliance_slope, val));
		getController().recv(Parameters::read_duration, getStatus());
	}
}

void RobotArm::changeIValue(int val, int servo_index) {
	if(servo_index == -1) {
		for(int i(2) ; i < 7 ; i++) {
			getController().send(dynamixel::ax12::WriteData(i, dynamixel::ax12::ctrl::ccw_compliance_margin, val));
			getController().recv(Parameters::read_duration, getStatus());
		}
	} else if(servo_index >= 2 && servo_index <= 6) {
		getController().send(dynamixel::ax12::WriteData(servo_index, dynamixel::ax12::ctrl::ccw_compliance_margin, val));
		getController().recv(Parameters::read_duration, getStatus());
	}
}

void RobotArm::changeDValue(int val, int servo_index) {
	if(servo_index == -1) {
		for(int i(2) ; i < 7 ; i++) {
			getController().send(dynamixel::ax12::WriteData(i, dynamixel::ax12::ctrl::cw_compliance_margin, val));
			getController().recv(Parameters::read_duration, getStatus());
		}
	} else if(servo_index >= 2 && servo_index <= 6) {
		getController().send(dynamixel::ax12::WriteData(servo_index, dynamixel::ax12::ctrl::cw_compliance_margin, val));
		getController().recv(Parameters::read_duration, getStatus());
	}
}

void RobotArm::close_usb_controllers() {
	getController().close_serial();  
}


