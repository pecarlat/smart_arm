#include "simu.hpp"
#include <iostream>


Simu::Simu(Data d) {
	// GLOBAL
	setStep(0.015); // Step in millisecondes
	setSpeed(1.4); // Speed of the robot (in radian per second)
	setTimeOfStab(500); // Time let to the arm to stabilize itself
	
	// USEFULL VARIABLES
	setCrash(false);
	setTotalTorque(0);
	setMin(Eigen::Vector3d(100, 100, 100));
	setMax(Eigen::Vector3d(0, 0, 0));
	setEffectorAngle(-1);
	setWristAngle(-1);
	
	// ENVIRONMENT AND ARM
	setEnv(boost::shared_ptr<ode::Environment_arm>(new ode::Environment_arm()));
	setArm(boost::shared_ptr<robot::Arm>(new robot::Arm(*getEnv(), Params_Simu::table::height)));
	
	// ENVIRONMENT INITIALIZATION
	getEnv()->set_gravity(0, 0, -9.81);
	getEnv()->add_table(*getEnv(),
							Params_Simu::table::length, Params_Simu::table::width, Params_Simu::table::height,
							Eigen::Vector3d(Params_Simu::table::dist_arm_x, Params_Simu::table::dist_arm_y, Params_Simu::table::height/2));
	getEnv()->add_cube(*getEnv(),
							Eigen::Vector3d(Params_Simu::cube::dist_arm_x, Params_Simu::cube::dist_arm_y, Params_Simu::table::height),
							Params_Simu::cube::size,
							d.getRealCubeMass(),
							d.getRealCubeRotation());
	
	for(int i(0) ; i < Params_Simu::baskets::nb_of_baskets ; i++) {
		Eigen::Vector3d pos;
		pos[0] = Params_Simu::table::length-((Params_Simu::table::length/2.0f)-Params_Simu::table::dist_arm_x)-(Params_Simu::baskets::length/2.0f);
		int sig;
		if(i < (Params_Simu::baskets::nb_of_baskets/2.0f)-0.5f) { sig = -1; }
		else if(i > (Params_Simu::baskets::nb_of_baskets/2.0f)-0.5f) { sig = 1; }
		else { sig = 0; }
		pos[1] = sig*((Params_Simu::baskets::width*(fabs(Params_Simu::baskets::nb_of_baskets-(1+2*i))/2.0f))+fabs((floor(fabs(i-((Params_Simu::baskets::nb_of_baskets-1)/2.0f)))*0.005+0.0025)));
		pos[2] = Params_Simu::table::height;
		
		getEnv()->add_basket(i, *getEnv(), pos, Params_Simu::baskets::length, Params_Simu::baskets::width, Params_Simu::baskets::height, Params_Simu::baskets::thickness);
	}
	
#ifdef GRAPHIC
	getArm()->accept(_visitor);
	getEnv()->accept_table(_visitor);
	getEnv()->accept_cube(_visitor);
	for(int i(0) ; i < Params_Simu::baskets::nb_of_baskets ; i++)
		getEnv()->accept_basket(_visitor, i);
#endif
}

Simu::~Simu() {
	
}

void Simu::launch(Arm_controller &ac) {
	// Quit if no controller
	assert(ac.getNbWaypoints() > 0);
	
	
	// Launch all waypoint with stabilization between them (stab time explains a lot the time of Map-Elite process)
	if(!stabilize(Params_Simu::global::intensityOfStab/2)) { return; }
	
	Eigen::Vector3d posCube(0, 0, 0);
	for(int i(-1) ; i < ac.getNbWaypoints()-1 ; i++) {
		std::cout << "[simulation] move from wp" << i << " to wp" << i+1 << std::endl;
		goToWaypoint(ac, ac.getWaypoint(i+1)); // From i to i+1 (i = -1 for origin)
		if(!stabilize(Params_Simu::global::intensityOfStab)) { return; }
	}
	
	if(!stabilizeCube(Params_Simu::global::intensityOfStab)) { return; }
	
	
	// Recover all data needed for (1) knowing the concerned cell and (2) the appropriated performance
	// (1) - 5 dims
	if(getEffectorAngle() == -1) {
		setEffectorAngle(0.5);
		setWristAngle(0.5);
	}
	// (1) - 6 dims
	Eigen::Vector3d diff;
	for(int i(0) ; i < 3 ; i++)
		diff[i] = getMax()[i]-getMin()[i];
	setDiffCoords(diff);
	
	// (2) - Variance or torque
	if(Params_Simu::performance::variance) {
		setPerformance(ac.findVariance());
		if(!Params_Simu::performance::mean)
			setPerformance(getPerformance()*ac.getNbWaypoints());
	} else {
		setPerformance(getTotalTorque());
		if(Params_Simu::performance::mean)
			setPerformance(getPerformance()/float(ac.getNbWaypoints()));
	}
	// (2) - Minimize
	if(Params_Simu::performance::pain && getEnv()->getTableTouched()) {
		if(Params_Simu::performance::painRatio == 1000)
			setPerformance(-Params_Simu::performance::painRatio);
		else {
			if(Params_Simu::performance::minimize)
				setPerformance(-(getPerformance()*Params_Simu::performance::painRatio));
			else
				setPerformance(getPerformance()/Params_Simu::performance::painRatio);
		}
	} else if(Params_Simu::performance::minimize) {
		setPerformance(-getPerformance());
	}
}

bool Simu::stabilize(int intensity) {
	bool stabilized(false);
	int stab(0);
	
	for (size_t s(0) ; s < getTimeOfStab() && !stabilized ; ++s) {
		Eigen::Vector3d posArm(getArm()->pos());
		
		next_step();
		if ((getArm()->pos() - posArm).norm() < 1e-4)
			stab++;
		else
			stab = 0;
		
		if (stab > intensity)
			stabilized = true;
	}
	
	if(!stabilized) {
		setCrash(true);
		return false;
	}
	
	return true;
}

bool Simu::stabilizeCube(int intensity) {
	bool stabilized(false);
	int stab(0);
	
	for (size_t s(0) ; s < getTimeOfStab() && !stabilized ; ++s) {
		Eigen::Vector3d posCube(getEnv()->getCubePos());
		
		next_step();
		if ((getEnv()->getCubePos() - posCube).norm() < 1e-3)
			stab++;
		else
			stab = 0;
		
		if (stab > intensity)
			stabilized = true;
	}
	
	if(!stabilized) {
		setCrash(true);
		return false;
	}
	
	return true;
}

waypoint Simu::getCurrentPos() {
	waypoint current;
	std::vector<float> tmp;
	for(int i(0) ; i < getArm()->servos().size() ; i++) {
		tmp.push_back((getArm()->servos()[i]->get_angle(0)/M_PI)+0.5);
	}
	
	current.step = tmp;
	current.fingersValue = 0;
	
	return current;
}

void Simu::goToWaypoint(Arm_controller ac, waypoint target) {
	
	int nbOfFingers(2), nbOfServosByFingers(1);
	int nbOfServosForFingers(nbOfFingers*nbOfServosByFingers);
	bool fingersDone(false);
	
	waypoint current = getCurrentPos();
	float moveTime(ac.getMoveTime(current, target, getSpeed()));
	
	Eigen::Vector3d initCubePos(getEnv()->getCubePos());
	
	// The motor that reached the longest distance is the reference : all the other motors has to do their move in the same time
	for (float t(0) ; t < moveTime ; t += getStep()) {
		for(int i(0) ; i < getArm()->servos().size()-nbOfServosForFingers ; i++) {
			float prevAngle(M_PI*(current.step[i]-0.5)), currAngle(M_PI*(target.step[i]-0.5));
			float newAngle(prevAngle + (currAngle-prevAngle)/moveTime*(t+getStep()));
			
			getArm()->servos()[i]->set_angle(0, newAngle);
		}
		
		// Fingers action (open / close) can be done at any moment of the move
		if(!fingersDone && (t/moveTime > target.fingersValue || t+getStep() >= moveTime)) {
			for(int i(0) ; i < nbOfFingers ; i++) {
				int posServo = getArm()->servos().size() - nbOfServosForFingers + 1;
				float currAngle(M_PI*(target.step[6+i]-0.5));
				
				getArm()->servos()[posServo + nbOfServosByFingers*i - 1]->set_angle(0, currAngle);
			}
			fingersDone = true;
		}
		
		next_step();
		
		
		// Check if the min / max position of the cube has changed and if the cube has moved
		Eigen::Vector3d pos(getEnv()->getCubePos());
		
		for(int i(0) ; i < 3 ; i++) {
			if(pos[i] < getMin()[i]) { getMin()[i] = pos[i]; }
			if(pos[i] > getMax()[i]) { getMax()[i] = pos[i]; }
		}
		
		if(getEffectorAngle() == -1 && (fabs(initCubePos[0]-pos[0]) > 1e-3 || 
										fabs(initCubePos[1]-pos[1]) > 1e-3 || 
										fabs(initCubePos[2]-pos[2]) > 1e-3)) {
			float sum = 0;
			for(int i(1) ; i < getArm()->servos().size()-nbOfServosForFingers-1 ; i++) {
				sum += getArm()->servos()[i]->get_angle(0)/float(M_PI);
			}
			sum = round((sum*(-1))*1e3)/float(1e3) - 0.5;
			if(sum < 0) sum = 0;
			else if(sum > 1) sum = 1;
			setEffectorAngle(sum);
			
			float wrist(getArm()->servos()[getArm()->servos().size()-nbOfServosForFingers-1]->get_angle(0)/float(M_PI) + 0.5);
			wrist = (wrist - 0.1) / 0.8f;
			wrist = round(wrist*1e3)/float(1e3);
			if(wrist < 0) wrist = 0;
			else if(wrist > 1) wrist = 1;
			setWristAngle(wrist);
		}
	}
	
	// Update the sum of torques
	for(int i(0) ; i < getArm()->servos().size() ; i++)
		setTotalTorque(getTotalTorque()+getArm()->servos()[i]->get_torque());
}

void Simu::next_step() {
	getArm()->next_step(getStep());
	getEnv()->next_step(getStep());
	
#ifdef GRAPHIC
	_visitor.update();
	usleep(1e4);
#endif
}



