#ifndef STAT_MAP_HPP_
#define STAT_MAP_HPP_

#include <numeric>
#include <boost/multi_array.hpp>
#include <sferes/stat/stat.hpp>

namespace sferes {
	namespace stat {
		SFERES_STAT(Map, Stat) {
		public:
			
			typedef boost::shared_ptr<Phen> phen_t;
			typedef boost::multi_array<phen_t, 3> array_t;
			typedef boost::array<float, 3> point_t;

			Map() : _xs(0), _ys(0), _zs(0) {}
			template<typename E>
	
			void refresh(const E& ea) {
				_archive.clear();
				_xs = ea.archive().shape()[0];
				_ys = ea.archive().shape()[1];
				_zs = ea.archive().shape()[2];
				assert(_xs == Params::ea_dim3::res_x);
				assert(_ys == Params::ea_dim3::res_y);
				assert(_zs == Params::ea_dim3::res_z);
				
				for(size_t i(0) ; i < _xs ; ++i)
					for(size_t j(0) ; j < _ys ; ++j)
						for (size_t k(0) ; k < _zs ; ++k) {
							phen_t p = ea.archive()[i][j][k];
							_archive.push_back(p);
						}
				
				if (ea.gen() % Params::pop::dump_period == 0) {
					_write_archive(ea.archive(), std::string("archive_"), ea);
#ifdef MAP_WRITE_PARENTS
					_write_parents(ea.archive(), ea.parents(), std::string("parents_"), ea);
#endif
				}
			}
			
			void show(std::ostream& os, size_t k) {
				std::cerr << "loading "<< k / _ys << "," << k % _ys << std::endl;
				if (_archive[k]) {
					_archive[k]->develop();
					_archive[k]->show(os);
					_archive[k]->fit().set_mode(fit::mode::view);
					_archive[k]->fit().eval(*_archive[k]);
				} else {
					std::cerr << "Warning, no point here" << std::endl;
				}
			}
			
			template<class Archive>
			void serialize(Archive& ar, const unsigned int version) {
				ar & BOOST_SERIALIZATION_NVP(_archive);
				ar & BOOST_SERIALIZATION_NVP(_xs);
				ar & BOOST_SERIALIZATION_NVP(_ys);
				ar & BOOST_SERIALIZATION_NVP(_zs);
			}
			
			
		protected:
			
			std::vector<phen_t> _archive;
			int _xs, _ys, _zs;

			template<typename EA>
			void _write_parents(const array_t& array,
								const array_t& p_array,
								const std::string& prefix,
								const EA& ea) const {
				std::cout << "writing..." << prefix << ea.gen() << std::endl;
				std::string fname =	ea.res_dir() + "/"
												+ prefix
												+ boost::lexical_cast<std::string>(ea.gen())
												+ std::string(".dat");
				std::ofstream ofs(fname.c_str());
				
				for (size_t i(0) ; i < _xs ; ++i)
					for (size_t j(0) ; j < _ys ; ++j)
						for (size_t k(0) ; k < _zs ; ++k)
							if (array[i][j][k] && p_array[i][j][k]) {
								point_t p = _get_point(p_array[i][j][k]);
								size_t x = round(p[0] * _xs);
								size_t y = round(p[1] * _ys);
								size_t z = round(p[2] * _zs);
								ofs << i / (float) _xs
									<< " " << j / (float) _ys
									<< " " << k / (float) _zs
									<< " " << p_array[i][j][k]->fit().value()
									<< " " << x / (float) _xs
									<< " " << y / (float) _ys
									<< " " << z / (float) _zs
									<< " " << array[i][j][j]->fit().value()
									<< std::endl;
							}
			}

			template<typename EA>
			void _write_archive(const array_t& array,
								const std::string& prefix,
								const EA& ea) const {
				std::cout << "writing..." << prefix << ea.gen() << std::endl;
				std::string fname = ea.res_dir() + "/"
												+ prefix
												+ boost::lexical_cast<std::string>(ea.gen())
												+ std::string(".dat");
				
				std::ofstream ofs(fname.c_str());
				for (size_t i(0) ; i < _xs ; ++i)
					for (size_t j(0) ; j < _ys ; ++j)
						for (size_t k(0) ; k < _zs ; ++k)
							if (array[i][j][k]) {
								ofs << i / (float) _xs
									<< " " << j / (float) _ys
									<< " " << k / (float) _zs
									<< "\t" << array[i][j][k]->fit().value()
									<< "\t";
								for(int a(0) ; a < array[i][j][k]->gen().size() ; a++) {
									ofs << array[i][j][k]->data(a) << " ";
								}
								ofs << std::endl;
							}
			}
		};
	}
}

#endif
