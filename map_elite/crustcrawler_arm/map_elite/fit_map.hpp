#include <sferes/fit/fitness.hpp>

#define FIT_MAP(Name) SFERES_FITNESS(Name, sferes::fit::FitMap)

namespace sferes {
	namespace fit {
		SFERES_FITNESS(FitMap, sferes::fit::Fitness) {
		public:
			FitMap() : _desc(Params::global::map_dim) { }
			const std::vector<float>& desc() const {
				return _desc;
			}
			void set_desc(std::vector<float> pos) {
				for(int i(0) ; i < Params::global::map_dim ; i++) {
					assert(pos[i] >= 0);
					assert(pos[i] <= 1);
					_desc[i] = pos[i];
				}
			}
		protected:
			std::vector<float> _desc;
		};
	}
}
