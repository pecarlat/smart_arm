#ifndef STAT_MAP_HPP_
#define STAT_MAP_HPP_

#include <numeric>
#include <boost/multi_array.hpp>
#include <sferes/stat/stat.hpp>

namespace sferes {
	namespace stat {
		SFERES_STAT(Map, Stat) {
		public:
			
			typedef boost::shared_ptr<Phen> phen_t;
			typedef boost::multi_array<phen_t, 6> array_t;
			typedef boost::array<float, 6> point_t;

			Map() : _xs(0), _ys(0), _zs(0), _dxs(0), _dys(0), _dzs(0) {}
			template<typename E>
	
			void refresh(const E& ea) {
				_archive.clear();
				_xs = ea.archive().shape()[0];
				_ys = ea.archive().shape()[1];
				_zs = ea.archive().shape()[2];
				_dxs = ea.archive().shape()[3];
				_dys = ea.archive().shape()[4];
				_dzs = ea.archive().shape()[5];
				assert(_xs == Params::ea_dim6::res_x);
				assert(_ys == Params::ea_dim6::res_y);
				assert(_zs == Params::ea_dim6::res_z);
				assert(_dxs == Params::ea_dim6::dif_x);
				assert(_dys == Params::ea_dim6::dif_y);
				assert(_dzs == Params::ea_dim6::dif_z);
				
				for(size_t i(0) ; i < _xs ; ++i)
					for(size_t j(0) ; j < _ys ; ++j)
						for (size_t k(0) ; k < _zs ; ++k)
							for (size_t l(0) ; l < _dxs ; ++l)
								for (size_t m(0) ; m < _dys ; ++m)
									for (size_t n(0) ; n < _dzs ; ++n) {
										phen_t p = ea.archive()[i][j][k][l][m][n];
										_archive.push_back(p);
									}
				
				if (ea.gen() % Params::pop::dump_period == 0) {
					_write_archive(ea.archive(), std::string("archive_"), ea);
#ifdef MAP_WRITE_PARENTS
					_write_parents(ea.archive(), ea.parents(), std::string("parents_"), ea);
#endif
				}
			}
			
			void show(std::ostream& os, size_t k) {
				std::cerr << "loading "<< k / _ys << "," << k % _ys << std::endl;
				if (_archive[k]) {
					_archive[k]->develop();
					_archive[k]->show(os);
					_archive[k]->fit().set_mode(fit::mode::view);
					_archive[k]->fit().eval(*_archive[k]);
				} else {
					std::cerr << "Warning, no point here" << std::endl;
				}
			}
			
			template<class Archive>
			void serialize(Archive& ar, const unsigned int version) {
				ar & BOOST_SERIALIZATION_NVP(_archive);
				ar & BOOST_SERIALIZATION_NVP(_xs);
				ar & BOOST_SERIALIZATION_NVP(_ys);
				ar & BOOST_SERIALIZATION_NVP(_zs);
				ar & BOOST_SERIALIZATION_NVP(_dxs);
				ar & BOOST_SERIALIZATION_NVP(_dys);
				ar & BOOST_SERIALIZATION_NVP(_dzs);
			}
			
			
		protected:
			
			std::vector<phen_t> _archive;
			int _xs, _ys, _zs, _dxs, _dys, _dzs;

			template<typename EA>
			void _write_parents(const array_t& array,
								const array_t& p_array,
								const std::string& prefix,
								const EA& ea) const {
				std::cout << "writing..." << prefix << ea.gen() << std::endl;
				std::string fname =	ea.res_dir() + "/"
												+ prefix
												+ boost::lexical_cast<std::string>(ea.gen())
												+ std::string(".dat");
				std::ofstream ofs(fname.c_str());
				
				for (size_t i(0) ; i < _xs ; ++i)
					for (size_t j(0) ; j < _ys ; ++j)
						for (size_t k(0) ; k < _zs ; ++k)
							for (size_t l(0) ; l < _dxs ; ++l)
								for (size_t m(0) ; m < _dys ; ++m)
									for (size_t n(0) ; n < _dzs ; ++n)
										if (array[i][j][k][l][m][n] && p_array[i][j][k][l][m][n]) {
											point_t p = _get_point(p_array[i][j][k][l][m][n]);
											size_t x = round(p[0] * _xs);
											size_t y = round(p[1] * _ys);
											size_t z = round(p[2] * _zs);
											size_t dx = round(p[3] * _dxs);
											size_t dy = round(p[4] * _dys);
											size_t dz = round(p[5] * _dzs);
											ofs << i / (float) _xs
												<< " " << j / (float) _ys
												<< " " << k / (float) _zs
												<< " " << l / (float) _dxs
												<< " " << m / (float) _dys
												<< " " << n / (float) _dzs
												<< " " << p_array[i][j][k][l][m][n]->fit().value()
												<< " " << x / (float) _xs
												<< " " << y / (float) _ys
												<< " " << z / (float) _zs
												<< " " << dx / (float) _dxs
												<< " " << dy / (float) _dys
												<< " " << dz / (float) _dzs
												<< " " << array[i][j][j][l][m][n]->fit().value()
												<< std::endl;
										}
			}

			template<typename EA>
			void _write_archive(const array_t& array,
								const std::string& prefix,
								const EA& ea) const {
				std::cout << "writing..." << prefix << ea.gen() << std::endl;
				std::string fname = ea.res_dir() + "/"
												+ prefix
												+ boost::lexical_cast<std::string>(ea.gen())
												+ std::string(".dat");
				
				std::ofstream ofs(fname.c_str());
				for (size_t i(0) ; i < _xs ; ++i)
					for (size_t j(0) ; j < _ys ; ++j)
						for (size_t k(0) ; k < _zs ; ++k)
							for (size_t l(0) ; l < _dxs ; ++l)
								for (size_t m(0) ; m < _dys ; ++m)
									for (size_t n(0) ; n < _dzs ; ++n)
										if (array[i][j][k][l][m][n]) {
											ofs << i / (float) _xs
												<< " " << j / (float) _ys
												<< " " << k / (float) _zs
												<< " " << l / (float) _dxs
												<< " " << m / (float) _dys
												<< " " << n / (float) _dzs
												<< "\t" << array[i][j][k][l][m][n]->fit().value()
												<< "\t";
											for(int a(0) ; a < array[i][j][k][l][m][n]->gen().size() ; a++) {
												ofs << array[i][j][k][l][m][n]->data(a) << " ";
											}
											ofs << std::endl;
										}
			}
		};
	}
}

#endif
