#ifndef MAP_ELITE_HPP_
#define MAP_ELITE_HPP_

#include <algorithm>
#include <limits>

#include <boost/foreach.hpp>
#include <boost/multi_array.hpp>
#include <boost/array.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/for_each.hpp>

#include <sferes/stc.hpp>
#include <sferes/ea/ea.hpp>
#include <sferes/fit/fitness.hpp>


namespace sferes {
	namespace ea {
		// Main class
		SFERES_EA(MapElite, Ea) {
		public:
			
			typedef boost::shared_ptr<Phen> indiv_t;
			typedef typename std::vector<indiv_t> pop_t;
			typedef typename std::vector<std::vector<indiv_t> > front_t;
			typedef boost::array<float, 6> point_t;
			typedef boost::shared_ptr<Phen> phen_t;
			typedef boost::multi_array<phen_t, 6> array_t;

			static const size_t res_x = Params::ea_dim6::res_x;
			static const size_t res_y = Params::ea_dim6::res_y;
			static const size_t res_z = Params::ea_dim6::res_z;
			static const size_t dif_x = Params::ea_dim6::dif_x;
			static const size_t dif_y = Params::ea_dim6::dif_y;
			static const size_t dif_z = Params::ea_dim6::dif_z;

			MapElite() :
				_array(boost::extents[res_x][res_y][res_z][dif_x][dif_y][dif_z]),
				_array_parents(boost::extents[res_x][res_y][res_z][dif_x][dif_y][dif_z]) {
			}

			void random_pop() {
				parallel::init();
				this->_pop.resize(Params::pop::init_size);
				BOOST_FOREACH(boost::shared_ptr<Phen>& indiv, this->_pop) {
					indiv = boost::shared_ptr<Phen>(new Phen());
					indiv->random();
					indiv->develop();
				}
				this->_eval_pop(this->_pop, 0, this->_pop.size());
				BOOST_FOREACH(boost::shared_ptr<Phen>&indiv, this->_pop)
					_add_to_archive(indiv, indiv);
			}

			void epoch() {
				this->_pop.clear();
				for (size_t i(0) ; i < res_x ; ++i)
					for (size_t j(0) ; j < res_y ; ++j)
						for (size_t k(0) ; k < res_z ; ++k)
							for (size_t l(0) ; l < dif_x ; ++l)
								for (size_t m(0) ; m < dif_y ; ++m)
									for (size_t n(0) ; n < dif_z ; ++n)
									if (_array[i][j][k][l][m][n])
										this->_pop.push_back(_array[i][j][k][l][m][n]);
				
				pop_t ptmp, p_parents;
				for (size_t i(0) ; i < Params::pop::size / 2 ; ++i) {
					indiv_t p1 = _selection(this->_pop);
					indiv_t p2 = _selection(this->_pop);
					boost::shared_ptr<Phen> i1, i2;
					p1->cross(p2, i1, i2);
					i1->mutate();
					i2->mutate();
					i1->develop();
					i2->develop();
					ptmp.push_back(i1);
					ptmp.push_back(i2);
					p_parents.push_back(p1);
					p_parents.push_back(p2);
				}
				this->_eval_pop(ptmp, 0, ptmp.size());

				assert(ptmp.size() == p_parents.size());
				for (size_t i(0) ; i < ptmp.size() ; ++i)
					_add_to_archive(ptmp[i], p_parents[i]);
			}

			const array_t& archive() const {
				return _array;
			}
			const array_t& parents() const {
				return _array_parents;
			}
			

		protected:

			bool _add_to_archive(indiv_t i1, indiv_t parent) {
				point_t p = _get_point(i1);
				size_t x = round(p[0] * res_x);
				size_t y = round(p[1] * res_y);
				size_t z = round(p[2] * res_z);
				size_t dx = round(p[3] * dif_x);
				size_t dy = round(p[4] * dif_y);
				size_t dz = round(p[5] * dif_z);
				
				x = std::min(x, res_x - 1);
				y = std::min(y, res_y - 1);
				z = std::min(z, res_z - 1);
				dx = std::min(dx, dif_x - 1);
				dy = std::min(dy, dif_y - 1);
				dz = std::min(dz, dif_z - 1);
				assert(x < res_x);
				assert(y < res_y);
				assert(z < res_z);
				assert(dx < dif_x);
				assert(dy < dif_y);
				assert(dz < dif_z);

				if (!_array[x][y][z][dx][dy][dz] || i1->fit().value() > _array[x][y][z][dx][dy][dz]->fit().value()) {
					_array[x][y][z][dx][dy][dz] = i1;
					_array_parents[x][y][z][dx][dy][dz] = parent;
					return true;
				}
				return false;
			}


			template<typename I>
			point_t _get_point(const I& indiv) {
				point_t p;
				p[0] = std::min(1.0f, indiv->fit().desc()[0]);
				p[1] = std::min(1.0f, indiv->fit().desc()[1]);
				p[2] = std::min(1.0f, indiv->fit().desc()[2]);
				p[3] = std::min(1.0f, indiv->fit().desc()[3]);
				p[4] = std::min(1.0f, indiv->fit().desc()[4]);
				p[5] = std::min(1.0f, indiv->fit().desc()[5]);

				return p;
			}

			indiv_t _selection(const pop_t& pop) {
				int x1 = misc::rand< int > (0, pop.size());
				return pop[x1];
			}
			
			
		private:
			
			array_t _array;
			array_t _prev_array;
			array_t _array_parents;
		};
	}
}
#endif
