// THIS IS A GENERATED FILE - DO NOT EDIT
#define TEXT
#line 1 "/home/pecarlat/dream/sferes2/exp/crustcrawler_arm/main.cpp"
#include <iostream>
#include <cmath>

#include <boost/test/unit_test.hpp>

#include <Eigen/Geometry>

#include <sferes/eval/parallel.hpp>
#include <sferes/gen/evo_float.hpp>
#include <sferes/gen/evo_float_mutable.hpp>
#include <sferes/phen/parameters.hpp>
#include <sferes/modif/dummy.hpp>
#include <sferes/run.hpp>
#include <sferes/stat/best_fit.hpp>

#include "simu.hpp"
#ifdef GRAPHIC
#include "real.hpp"
#endif
#include "arm_controller.hpp"
#include "parameters_simulation.hpp"
//#include "map_elite/3dims.h"
//#include "map_elite/5dims.h"
#include "map_elite/6dims.h"
#include "map_elite/parameters_mutable.hpp"

#include "tbb/tbb.h"

#ifdef GRAPHIC
#define NO_PARALLEL
#include "renderer/osg_visitor.hh"
#endif




using namespace sferes::gen::evo_float;
using namespace sferes;


struct Params {
	// Global parameters
	struct global {
		/* Dimension of the map, can be :
		 *	- 3 : (x, y, z) -> final position of the cube, corresponds to ea_dim3
		 *	- 5 : (x, y, z, a, w) -> final position of the cube + diversity criteria (angle of the gripper whe grasping the cube + rotation of the wrist), corresponds to ea_dim5
		 *	- 6 : (x, y, z, dx, dy, dz) -> final position of the cube + trajectory of the cube (dx = max(cube_x) - min(cube_x)), corresponds to ea_dim6
		 */
		static const int map_dim = 6; // Have to change the "include" call
		static const int waypoint_size = 7; // Size of a waypoint
	};
	
	// Size of map's dimensions
	struct ea_dim3 {
		// 24*16*20 = 7680 cells -> used by map_elite/3dims folder
		SFERES_CONST size_t res_x = 24;
		SFERES_CONST size_t res_y = 16;
		SFERES_CONST size_t res_z = 20;
	};
	struct ea_dim5 {
		// 24*16*20*10*8 = 614400 cells -> used by map_elite/5dims folder
		SFERES_CONST size_t res_x = 24;
		SFERES_CONST size_t res_y = 16;
		SFERES_CONST size_t res_z = 20;
		SFERES_CONST size_t angle = 10;
		SFERES_CONST size_t wrist = 8;
	};
	struct ea_dim6 {
		// 24*16*20*6*4*5 = 921600 cells -> used by map_elite/6dims folder
		SFERES_CONST size_t res_x = 24;
		SFERES_CONST size_t res_y = 16;
		SFERES_CONST size_t res_z = 20;
		SFERES_CONST size_t dif_x = 6;
		SFERES_CONST size_t dif_y = 4;
		SFERES_CONST size_t dif_z = 5;
	};
	
	// Variables about the population
	struct pop {
		SFERES_CONST size_t init_size = 400; // Size needed for building the first map
		SFERES_CONST size_t size = 200; // Size of the population
		SFERES_CONST size_t nb_gen = 10001; // Number of generations
		SFERES_CONST size_t dump_period = 50; // Number of generation needed before being writed in archives
	};
	// Values min/max of parameters (all data provided by (or given to) map-elite will (has to) be between min and max)
	struct parameters {
		SFERES_CONST float min = 0;
		SFERES_CONST float max = 1;
	};
	// Variables about map-elite
	struct evo_float {
		SFERES_CONST cross_over_t cross_over_type = sbx; // Cross-over type (here polynomial)
		SFERES_CONST float cross_rate = 0.0f; // Cross-rate of the real-valued vector
		SFERES_CONST mutation_t mutation_type = polynomial; // Mutation type
		SFERES_CONST float mutation_rate = 0.1f; // Mutation rate of the real-valued vector
		SFERES_CONST float eta_m = 10.0f; // A parameter of the polynomial mutation
		SFERES_CONST float eta_c = 10.0f; // A parameter of the polynomial cross-over
		SFERES_CONST float modify_genotype = 0.15f; // Probability of genotype's mutation
		SFERES_CONST float extend_delete_prob = 0.5f; // Probability of mutation by deletion
	};
};


// Some additionnal informations about the simulation (see parameters_simulation.hpp)
Data d;


// Fitness function
FIT_MAP(Fit_eval) {
	
public:
	
	template<typename Indiv>
	void eval(Indiv& ind) {
		// Round random values to x numbers after dot (avoid long float / double precision problem)
		int discretization = Params_Simu::global::discretization;
		
		// Recover data
		int nb_wp = ind.size()/Params::global::waypoint_size;
		std::vector<float> wp[nb_wp];
		
		for(int i(0) ; i < Params::global::waypoint_size ; i++) {
			for(int j(0) ; j < nb_wp ; j++)
				wp[j].push_back(round(ind.data(i+j*Params::global::waypoint_size)*discretization)/float(discretization));
		}
		
		// Put them into our controller
		Arm_controller ac;
		ac.setOrigin(0.5f);
		for(int i(0) ; i < nb_wp ; i++)
			ac.addWaypoint(wp[i]);
		ac.showControllers();
		std::cout << std::endl;
		
	 	// Launch simulation
		std::cout << "[simulation] caracts :" << std::endl;
		Simu simu(d);
		
		try {
			simu.launch(ac);
		} catch(int e) {
			std::cout << "[simulation] exception met !" << std::endl;
			simu.setCrash(true);
		}
		
		// Recover and process cube position
		if(!simu.getCrash()) {
			Eigen::Vector3d now(simu.getEnv()->getCubePos()); // Absolute position of the cube after experiment
			now[0] = (now[0] - d.getBorder_left_x()) / Params_Simu::grid::size_x;
			now[1] = (now[1] - d.getBorder_left_y()) / Params_Simu::grid::size_y;
			now[2] = (now[2] - d.getBorder_left_z()) / Params_Simu::grid::size_z;
			
			if(now[0] >= 0 && now[1] >= 0 && now[2] >= 0 && now[0] <= 1 && now[1] <= 1 && now[2] <= 1) {
				std::vector<float> tmpCell;
				for(int i(0) ; i < 3 ; i++)
					tmpCell.push_back(now[i]);
				
				if(Params::global::map_dim == 5) {
					tmpCell.push_back(simu.getEffectorAngle());
					tmpCell.push_back(simu.getWristAngle());
				} else if(Params::global::map_dim == 6) {
					for(int i(0) ; i < 3 ; i++)
						tmpCell.push_back(simu.getDiffCoords()[i]);
				}
				
				std::cout << "[simulation] final cube position is in the grid" << std::endl;
				this->set_desc(tmpCell); // The parameters are the new cell
				this->_value = simu.getPerformance(); // Appropriated performance
			} else {
				std::cout << "[simulation] final cube position is NOT in the grid" << std::endl;
			}
			
			std::cout << "[simulation] final position : " << now[0] << "/" << now[1] << "/" << now[2] << std::endl;
			std::cout << "[simulation] final performance : " << simu.getPerformance() << std::endl;
			std::cout << "[simulation] final differences : " << simu.getDiffCoords()[0] << "/" << simu.getDiffCoords()[1] << "/" << simu.getDiffCoords()[2] << std::endl;
			std::cout << "[simulation] final angle found for effector : " << simu.getEffectorAngle() << std::endl;
			std::cout << "[simulation] final wrist angle found for effector : " << simu.getWristAngle() << std::endl;
			std::cout << std::endl;
		}
	}
};



int main(int argc, char** argv) {
	typedef Fit_eval<Params> fit_t;
	typedef gen::EvoFloatMutable<Params::global::waypoint_size, Params> gen_t; // 7*x parameters in the genotype (7 floats per waypoints, x waypoints)
	typedef phen::ParametersMutable<gen_t, fit_t, Params> phen_t;
#ifdef GRAPHIC
	typedef eval::Eval<Params> eval_t; // Single core
#else
	typedef eval::Parallel<Params> eval_t; // Using all cores
#endif
	typedef boost::fusion::vector<stat::Map<phen_t, Params>, stat::BestFit<phen_t, Params> > stat_t;
	typedef modif::Dummy<> modifier_t;
	typedef ea::MapElite<phen_t, eval_t, stat_t, modifier_t, Params> ea_t;
	
	
	dInitODE();
	srand(time(NULL));
	
	
	/******************************************************************************
	 * Map-Elite
	 * - exploration of the grid
	 */
	if(argc == 1) {
		std::cout << "[general] launched with Map-Elite -> exploration" << std::endl;
		std::cout << "[general] Map-Elite more important parameters :" << std::endl;
		std::cout << "[general] \tthe map has " << Params::global::map_dim << " dimensions" << std::endl;
		std::cout << "[general] \tthe chosen performance is the ";
			if(Params_Simu::performance::mean) std::cout << "mean"; else std::cout << "sum";
		std::cout << " of ";
			if(Params_Simu::performance::variance) std::cout << "variances"; else std::cout << "torques";
			if(Params_Simu::performance::pain) std::cout << " with punition (pain ratio = " << Params_Simu::performance::painRatio << ")" << std::endl;
		std::cout << std::endl;
		
		ea_t ea;
		ea.run();
	}
	
	
	/******************************************************************************
	 * Sample
	 * - test of a sample found by Map-Elite (variable number of waypoints
	 * - simulation and real launched
	 */
	else if((argc-1)%Params::global::waypoint_size == 0) { // Simulation of a Map-Elite result ; needs a multiple of 7 arguments
		std::cout << "[general] test of a sample found by Map-Elite" << std::endl;
		std::cout << "[general] experiment made in the following grid :" << std::endl;
		std::cout << "[general] \tgrid x : " << d.getBorder_left_x() << " <= x <= " << d.getBorder_right_x() << std::endl;
		std::cout << "[general] \tgrid y : " << d.getBorder_left_y() << " <= y <= " << d.getBorder_right_y() << std::endl;
		std::cout << "[general] \tgrid z : " << d.getBorder_left_z() << " <= z <= " << d.getBorder_right_z() << std::endl;
		std::cout << "[general] \tRQ, position of the arm : (0 ; 0 ; " << Params_Simu::table::height << ") " << std::endl;
		std::cout << std::endl;
		
		
		// Round random values to x numbers after dot (avoid long float / double precision problem)
		int discretization = Params_Simu::global::discretization;
		
		// Recover data
		int nb_wp = (argc-1)/Params::global::waypoint_size;
		std::vector<float> wp[nb_wp];
		for(int i(0) ; i < Params::global::waypoint_size ; i++) {
			for(int j(0) ; j < nb_wp ; j++) {
				wp[j].push_back(round(std::atof(argv[1+i+j*(Params::global::waypoint_size)])*discretization)/float(discretization));
			}
		}
		
		// Put them into our controller
		Arm_controller ac;
		ac.setOrigin(0.5f);
		for(int i(0) ; i < nb_wp ; i++)
			ac.addWaypoint(wp[i]);
		ac.showControllers();
		std::cout << std::endl;
		
		// Launch simulation
		std::cout << "[simulation] caracts :" << std::endl;
		Simu simu(d);
		try {
			simu.launch(ac);
		} catch(int e) {
			std::cout << "[simulation] exception met !" << std::endl;
		}
		
		// Recover and process cube position
		if(!simu.getCrash()) {
			Eigen::Vector3d now(simu.getEnv()->getCubePos()); // Absolute position of the cube after experiment
			now[0] = (now[0] - d.getBorder_left_x()) / Params_Simu::grid::size_x;
			now[1] = (now[1] - d.getBorder_left_y()) / Params_Simu::grid::size_y;
			now[2] = (now[2] - d.getBorder_left_z()) / Params_Simu::grid::size_z;
			
			if(now[0] >= 0 && now[1] >= 0 && now[2] >= 0 && now[0] <= 1 && now[1] <= 1 && now[2] <= 1) {
				std::cout << "[simulation] final cube position is in the grid" << std::endl;
			} else {
				std::cout << "[simulation] final cube position is NOT in the grid" << std::endl;
			}
			
			std::cout << "[simulation] final position : " << now[0] << "/" << now[1] << "/" << now[2] << std::endl;
			std::cout << "[simulation] final performance : " << simu.getPerformance() << std::endl;
			std::cout << "[simulation] final differences : " << simu.getDiffCoords()[0] << "/" << simu.getDiffCoords()[1] << "/" << simu.getDiffCoords()[2] << std::endl;
			std::cout << "[simulation] final angle found for effector : " << simu.getEffectorAngle() << std::endl;
			std::cout << "[simulation] final wrist angle found for effector : " << simu.getWristAngle() << std::endl;
			std::cout << std::endl;
		}
		/*
		// Launch real model if initialization works (if dynamixels are detected)
		std::cout << "[real_arm] launch of this controller on the real arm" << std::endl;
		Real real;
		if(!real.getArm().getCrash()) {
			std::cout << "[real_arm] new controllers" << std::endl;
			ac.adaptToReality(); // Some adjustements for the real arm
			ac.showControllers();
			std::cout << std::endl;
			real.launch(ac);
		}*/
		
	} else {
		std::cout << "Error.. Please enter 0 argument (map-elite ; main text) or a multiple of " << Params::global::waypoint_size << " arguments (result of map-elite ; main graphic)" << std::endl;
	}
	
	dCloseODE();
	
	return 0;
}


